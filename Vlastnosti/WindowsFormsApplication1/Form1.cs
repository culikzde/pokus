﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        MyObject obj;

        public Form1()
        {
            InitializeComponent();
            obj = new MyObject();
            obj.X = 7;
            obj.Text = "Abc";
            propertyGrid1.SelectedObject = obj;
        }

        private void testToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView1.ShowItemToolTips = true;

            ListViewItem item = new ListViewItem();
            item.Text = "Klm";
            item.ToolTipText = "Nápověda";
            item.SubItems.Add("sloupec 2");
            item.SubItems.Add("sloupec 3");
            listView1.Items.Add(item);
        }

        private void showControls(Control c)
        {
            foreach (Control s in c.Controls)
            {
                ListViewItem item = new ListViewItem();
                item.Text = s.Name;
                item.ToolTipText = s.GetType().ToString();
                item.SubItems.Add(s.GetType().Name);
                item.SubItems.Add(s.GetType().ToString());
                item.Tag = s;
                listView1.Items.Add(item);

                showControls(s);
            }
        }

        public void componentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showControls(this);
        }

        [ Category ("Moje") ]
        public void reflectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            object p = this;
            System.Type t = p.GetType();
            System.Reflection.MethodInfo[] m = t.GetMethods();
            foreach (System.Reflection.MethodInfo n in m)
            {
                ListViewItem item = new ListViewItem();
                item.Text = n.Name;
                item.ToolTipText = n.ToString();
                item.SubItems.Add(n.ReturnType.Name);
                item.SubItems.Add(n.ToString());

                object[] a = n.GetCustomAttributes(false);
                foreach (object b in a)
                    item.SubItems.Add(b.GetType().Name);

                item.Tag = n;
                listView1.Items.Add(item);
            }
        }

        public void zelena()
        {
            BackColor = Color.Green;
        }

        public void cervena()
        {
            BackColor = Color.Red;
        }

        public void zelenaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            zelena();
        }

        public void cervenaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cervena();
        }

        public void secti(int a, int b)
        {
            MessageBox.Show("vysledek je " + (a + b));
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 1)
            {
                object p = listView1.SelectedItems[0].Tag;
                propertyGrid1.SelectedObject = p;
                if (p is System.Reflection.MethodInfo)
                {
                    System.Reflection.MethodInfo m = (System.Reflection.MethodInfo)p;
                    listView2.Items.Clear();
                    foreach (System.Reflection.ParameterInfo u in m.GetParameters())
                    {
                        ListViewItem item = new ListViewItem();
                        item.Text = u.Name;
                        item.ToolTipText = u.ToString();
                        item.SubItems.Add(u.ParameterType.Name);
                        item.Tag = u;
                        listView2.Items.Add(item);
                    }
                }
            }
            else
                propertyGrid1.SelectedObject = null;
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 1)
            {
                object p = listView1.SelectedItems[0].Tag;
                if (p is System.Reflection.MethodInfo)
                {
                    System.Reflection.MethodInfo m = (System.Reflection.MethodInfo)p;
                    if (m.GetParameters().Length == 0)
                        m.Invoke(this, new object[] { });
                    if (m.GetParameters().Length == 2)
                    {
                        System.Reflection.ParameterInfo u = m.GetParameters()[0];
                        System.Reflection.ParameterInfo v = m.GetParameters()[1];
                        if (u.ParameterType.Name == "Int32" &&
                            v.ParameterType.Name == "Int32")

                        m.Invoke(this, new object[] { 1, 2 });
                        else 
                        m.Invoke(this, new object[] { null, null });
                    }
                }
            }
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listView2.SelectedItems.Count == 1)
                propertyGrid1.SelectedObject = listView2.SelectedItems[0].Tag;
        }

    }
}
