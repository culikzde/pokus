﻿
using System.ComponentModel;

namespace WindowsFormsApplication1
{
    class MyObject
    {
        private int x;

        // using System.ComponentModel;
        [ Category ("Souřadnice") , 
          Description ("x-ová souřadnice") ]
        // [ Browsable (false) ]
        public int X
        {
            get { return x; }
            set { x = value; }
        }

        private string text;

        [ Category ("Popisy") ]
        [ Description ("nadpis") ]
        public string Text
        {
            get { return text; }
            set { text = value; }
        }
    }
}
