﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace DrawObjects
{
   public partial class MainForm : Form
   {
      private XmlDocument doc = null;

      private MyObject obj = null;

      public MainForm()
      {
         InitializeComponent ();
      }

      private void quitMenuItem_Click (object sender, EventArgs e)
      {
         Close ();
      }

      /* Tree */

      private MyTreeNode CurrentTreeObject()
      {
          return treeView.SelectedNode as MyTreeNode;
      }

      private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
      {
          MyTreeNode node = CurrentTreeObject();

          /* property grid */
          propertyGrid.SelectedObject = (node == null) ? null : node.Data;
      }


      /* XML file */

      private void AddXmlNode (TreeNode top, XmlNode node)
      {
         MyTreeNode item = new MyTreeNode ();
         item.Data = node;

         if (node is XmlElement)
            item.Text = node.Name;
         else
            item.Text = node.Value;

         if ( top == null )
            treeView.Nodes.Add (item);
         else
            top.Nodes.Add (item);

         foreach ( XmlNode n in node.ChildNodes )
            AddXmlNode (item, n);
      }

      private void openXmlMenuItem_Click (object sender, EventArgs e)
      {
         if ( openFileDialog.ShowDialog () == DialogResult.OK )
         {
            string fileName = openFileDialog.FileName;

            doc = new XmlDocument ();
            doc.Load (fileName);
           
            XmlElement elem = doc.DocumentElement;
            AddXmlNode (null, elem);
         }
      }

      private void saveXmlMenuItem_Click (object sender, EventArgs e)
      {
         if ( doc != null )
             if ( saveFileDialog.ShowDialog () == DialogResult.OK )
                doc.Save (saveFileDialog.FileName);
      }

      /* XML as Text */

      private void toTextMenuItem_Click(object sender, EventArgs e)
      {
          if (doc != null)
          {
              TextWriter w = new StringWriter();
              doc.Save(w);
              textBox.Text = w.ToString();
          }
      }

      private void fromTextMenuItem_Click(object sender, EventArgs e)
      {
          try
          {
              doc = new XmlDocument();
              doc.LoadXml(textBox.Text);
              XmlElement elem = doc.DocumentElement;
              AddXmlNode(null, elem);
          }
          catch (Exception ex)
          {
              statusLabel.Text = ex.Message;
          }

      }

      /* XML Object */

      private void AddObject (MyObject p)
      {
          obj = p;

          MyTreeNode n = new MyTreeNode();
          n.Text = "my object";
          n.Data = obj;

          treeView.Nodes.Add(n);
      }

      private void createObjectMenuItem_Click(object sender, EventArgs e)
      {
          AddObject (new MyObject ());
      }

      private void objectToTextMenuItem_Click(object sender, EventArgs e)
      {
          if (obj != null)
          {
              StringWriter stream = new StringWriter();

              XmlSerializer writer = new XmlSerializer(typeof(MyObject));
              writer.Serialize(stream, obj);
              
              textBox.Text = stream.ToString ();
          }

      }

      private void objectFromTextMenuItem_Click(object sender, EventArgs e)
      {
          StringReader stream = new StringReader(textBox.Text);

          XmlSerializer reader = new XmlSerializer(typeof(MyObject));
          obj = (MyObject)reader.Deserialize(stream);

          AddObject(obj);
      }

      private void loadObjectMenuItem_Click(object sender, EventArgs e)
      {
          if (openFileDialog.ShowDialog() == DialogResult.OK)
          {
              StreamReader stream = new StreamReader(openFileDialog.FileName);
              XmlSerializer reader = new XmlSerializer(typeof(MyObject));
              obj = (MyObject) reader.Deserialize(stream);
              stream.Close();

              AddObject(obj);
          }

      }

      private void saveObjectMenuItem_Click(object sender, EventArgs e)
      {
          if (saveFileDialog.ShowDialog() == DialogResult.OK)
          {
              StreamWriter stream = new StreamWriter(saveFileDialog.FileName);
              XmlSerializer writer = new XmlSerializer(typeof(MyObject));
              writer.Serialize(stream, obj);
              stream.Close();
          }
      }

      /* XML Data Source */

      /* Vlozit DataFrid View
       * V rohu vybrat malou znacku - Choose Data Source
       *    Other Data Sources
       *       Project Data Sources
       *          MyDataSource
       *              MyTable <-- az po tabulku
       */

      private void loadDataMenuItem_Click(object sender, EventArgs e)
      {
          if (openFileDialog.ShowDialog() == DialogResult.OK)
              myDataSet.ReadXml(openFileDialog.FileName);
      }

      private void saveDataMenuItem_Click(object sender, EventArgs e)
      {
          if (saveFileDialog.ShowDialog() == DialogResult.OK)
          {
              myDataSet.WriteXml(saveFileDialog.FileName);
          }
      }
      
   }
}
