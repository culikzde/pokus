﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Drawing;

// http://www.codeproject.com
// Using the XmlSerializer Attributes

namespace DrawObjects
{
   [XmlRoot ("OutData")]
   // [XmlInclude(typeof (Color))]
   public class MyObject
   {


      private string name = "abc";

      public string Name
      {
         get { return name; }
         set { name = value; }
      }

      private int value = 7;

      public int Value
      {
         get { return this.value; }
         set { this.value = value; }
      }

      private SubObject inner = new SubObject();

      [XmlElement("Simple")]
      public SubObject Inner
      {
          get { return inner; }
          set { inner = value; }
      }

      private AnotherObject other = new AnotherObject();

      // [XmlElement("Another")]
      public AnotherObject Other
      {
          get { return other; }
          set { other = value; }
      }

      private List<String> list1 = new List<String>() { "aaa", "bbb" };

      public List<String> List1
      {
          get { return list1; }
          set { list1 = value; }
      }

      private List<String> list2 = new List<String>() { "ccc", "ddd" };

     [ XmlArray ("Sequence"), XmlArrayItem ("Item") ]
      public List<String> List2
      {
          get { return list2; }
          set { list2 = value; }
      }

     private Color color = Color.Blue;
  
     [ XmlIgnore () ]
     public Color Color
     {
         get { return color; }
         set { color = value; }
     }

     [XmlElement("Color")]
     public String TextColor
       {
           get { return color.Name; }
           set { color = Color.FromName (value); }
       }
   }

   public class SubObject
   {
       private string name = "klm";

       [XmlAttribute ("Id")]
       public string Name
       {
           get { return name; }
           set { name = value; }
       }

       private int value = 7;

       [XmlAttribute("Num")]
       public int Value
       {
           get { return this.value; }
           set { this.value = value; }
       }
   }

   public class AnotherObject
   {
       private string name = "xyz";

       [XmlElement ("Identifier")]
       public string Name
       {
           get { return name; }
           set { name = value; }
       }

       private int value = 8;

       [XmlElement("Number")]
       public int Value
       {
           get { return this.value; }
           set { this.value = value; }
       }

   }
}
