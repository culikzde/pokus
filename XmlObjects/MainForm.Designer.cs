﻿namespace DrawObjects
{
   partial class MainForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose (bool disposing)
      {
         if ( disposing && (components != null) )
         {
            components.Dispose ();
         }
         base.Dispose (disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent ()
      {
          this.components = new System.ComponentModel.Container();
          System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
          this.menu = new System.Windows.Forms.MenuStrip();
          this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
          this.openXmlMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.saveXmlMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.toTextMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.fromTextMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.quitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.objectMenu = new System.Windows.Forms.ToolStripMenuItem();
          this.createObjectMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.objectToTextMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.objectFromTextMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.loadObjectMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.saveObjectMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.status = new System.Windows.Forms.StatusStrip();
          this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
          this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
          this.firstSplitContainer = new System.Windows.Forms.SplitContainer();
          this.treeView = new System.Windows.Forms.TreeView();
          this.textBox = new System.Windows.Forms.TextBox();
          this.secondSplitContainer = new System.Windows.Forms.SplitContainer();
          this.dataGrid = new System.Windows.Forms.DataGridView();
          this.propertyGrid = new System.Windows.Forms.PropertyGrid();
          this.toolbar = new System.Windows.Forms.ToolStrip();
          this.createObjectToolButton = new System.Windows.Forms.ToolStripButton();
          this.centralPanel = new System.Windows.Forms.Panel();
          this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
          this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
          this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
          this.dataMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.loadDataMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.saveDataMenuItem = new System.Windows.Forms.ToolStripMenuItem();
          this.firstDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
          this.secondDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
          this.myDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
          this.myDataSet = new DrawObjects.MyDataSet();
          this.menu.SuspendLayout();
          this.status.SuspendLayout();
          this.firstSplitContainer.Panel1.SuspendLayout();
          this.firstSplitContainer.Panel2.SuspendLayout();
          this.firstSplitContainer.SuspendLayout();
          this.secondSplitContainer.Panel1.SuspendLayout();
          this.secondSplitContainer.Panel2.SuspendLayout();
          this.secondSplitContainer.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).BeginInit();
          this.toolbar.SuspendLayout();
          this.centralPanel.SuspendLayout();
          this.mainSplitContainer.Panel1.SuspendLayout();
          this.mainSplitContainer.Panel2.SuspendLayout();
          this.mainSplitContainer.SuspendLayout();
          ((System.ComponentModel.ISupportInitialize)(this.myDataSetBindingSource)).BeginInit();
          ((System.ComponentModel.ISupportInitialize)(this.myDataSet)).BeginInit();
          this.SuspendLayout();
          // 
          // menu
          // 
          this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.objectMenu,
            this.dataMenuItem});
          this.menu.Location = new System.Drawing.Point(0, 0);
          this.menu.Name = "menu";
          this.menu.Size = new System.Drawing.Size(680, 24);
          this.menu.TabIndex = 0;
          this.menu.Text = "menuStrip1";
          // 
          // fileMenu
          // 
          this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openXmlMenuItem,
            this.saveXmlMenuItem,
            this.toTextMenuItem,
            this.fromTextMenuItem,
            this.quitMenuItem});
          this.fileMenu.Name = "fileMenu";
          this.fileMenu.ShortcutKeyDisplayString = "";
          this.fileMenu.Size = new System.Drawing.Size(35, 20);
          this.fileMenu.Text = "&File";
          // 
          // openXmlMenuItem
          // 
          this.openXmlMenuItem.Name = "openXmlMenuItem";
          this.openXmlMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
          this.openXmlMenuItem.Size = new System.Drawing.Size(173, 22);
          this.openXmlMenuItem.Text = "Open XML";
          this.openXmlMenuItem.Click += new System.EventHandler(this.openXmlMenuItem_Click);
          // 
          // saveXmlMenuItem
          // 
          this.saveXmlMenuItem.Name = "saveXmlMenuItem";
          this.saveXmlMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
          this.saveXmlMenuItem.Size = new System.Drawing.Size(173, 22);
          this.saveXmlMenuItem.Text = "Save XML";
          this.saveXmlMenuItem.Click += new System.EventHandler(this.saveXmlMenuItem_Click);
          // 
          // toTextMenuItem
          // 
          this.toTextMenuItem.Name = "toTextMenuItem";
          this.toTextMenuItem.Size = new System.Drawing.Size(173, 22);
          this.toTextMenuItem.Text = "To Text";
          this.toTextMenuItem.Click += new System.EventHandler(this.toTextMenuItem_Click);
          // 
          // fromTextMenuItem
          // 
          this.fromTextMenuItem.Name = "fromTextMenuItem";
          this.fromTextMenuItem.Size = new System.Drawing.Size(173, 22);
          this.fromTextMenuItem.Text = "From Text";
          this.fromTextMenuItem.Click += new System.EventHandler(this.fromTextMenuItem_Click);
          // 
          // quitMenuItem
          // 
          this.quitMenuItem.Name = "quitMenuItem";
          this.quitMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
          this.quitMenuItem.Size = new System.Drawing.Size(173, 22);
          this.quitMenuItem.Text = "&Quit";
          this.quitMenuItem.Click += new System.EventHandler(this.quitMenuItem_Click);
          // 
          // objectMenu
          // 
          this.objectMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createObjectMenuItem,
            this.objectToTextMenuItem,
            this.objectFromTextMenuItem,
            this.loadObjectMenuItem,
            this.saveObjectMenuItem});
          this.objectMenu.Name = "objectMenu";
          this.objectMenu.Size = new System.Drawing.Size(51, 20);
          this.objectMenu.Text = "&Object";
          // 
          // createObjectMenuItem
          // 
          this.createObjectMenuItem.Name = "createObjectMenuItem";
          this.createObjectMenuItem.Size = new System.Drawing.Size(153, 22);
          this.createObjectMenuItem.Text = "Create Object";
          this.createObjectMenuItem.Click += new System.EventHandler(this.createObjectMenuItem_Click);
          // 
          // objectToTextMenuItem
          // 
          this.objectToTextMenuItem.Name = "objectToTextMenuItem";
          this.objectToTextMenuItem.Size = new System.Drawing.Size(153, 22);
          this.objectToTextMenuItem.Text = "To Text";
          this.objectToTextMenuItem.Click += new System.EventHandler(this.objectToTextMenuItem_Click);
          // 
          // objectFromTextMenuItem
          // 
          this.objectFromTextMenuItem.Name = "objectFromTextMenuItem";
          this.objectFromTextMenuItem.Size = new System.Drawing.Size(153, 22);
          this.objectFromTextMenuItem.Text = "From Text";
          this.objectFromTextMenuItem.Click += new System.EventHandler(this.objectFromTextMenuItem_Click);
          // 
          // loadObjectMenuItem
          // 
          this.loadObjectMenuItem.Name = "loadObjectMenuItem";
          this.loadObjectMenuItem.Size = new System.Drawing.Size(153, 22);
          this.loadObjectMenuItem.Text = "Load from File";
          this.loadObjectMenuItem.Click += new System.EventHandler(this.loadObjectMenuItem_Click);
          // 
          // saveObjectMenuItem
          // 
          this.saveObjectMenuItem.Name = "saveObjectMenuItem";
          this.saveObjectMenuItem.Size = new System.Drawing.Size(153, 22);
          this.saveObjectMenuItem.Text = "Save to File";
          this.saveObjectMenuItem.Click += new System.EventHandler(this.saveObjectMenuItem_Click);
          // 
          // status
          // 
          this.status.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel,
            this.toolStripStatusLabel1});
          this.status.Location = new System.Drawing.Point(0, 425);
          this.status.Name = "status";
          this.status.Size = new System.Drawing.Size(680, 22);
          this.status.TabIndex = 1;
          this.status.Text = "statusStrip1";
          // 
          // statusLabel
          // 
          this.statusLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
          this.statusLabel.Name = "statusLabel";
          this.statusLabel.Size = new System.Drawing.Size(0, 17);
          // 
          // toolStripStatusLabel1
          // 
          this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
          this.toolStripStatusLabel1.Size = new System.Drawing.Size(62, 17);
          this.toolStripStatusLabel1.Text = "statusLabel";
          // 
          // firstSplitContainer
          // 
          this.firstSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
          this.firstSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
          this.firstSplitContainer.Location = new System.Drawing.Point(0, 0);
          this.firstSplitContainer.Name = "firstSplitContainer";
          // 
          // firstSplitContainer.Panel1
          // 
          this.firstSplitContainer.Panel1.Controls.Add(this.treeView);
          // 
          // firstSplitContainer.Panel2
          // 
          this.firstSplitContainer.Panel2.Controls.Add(this.textBox);
          this.firstSplitContainer.Size = new System.Drawing.Size(327, 376);
          this.firstSplitContainer.SplitterDistance = 161;
          this.firstSplitContainer.TabIndex = 3;
          // 
          // treeView
          // 
          this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
          this.treeView.Location = new System.Drawing.Point(0, 0);
          this.treeView.Name = "treeView";
          this.treeView.Size = new System.Drawing.Size(161, 376);
          this.treeView.TabIndex = 0;
          this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterSelect);
          // 
          // textBox
          // 
          this.textBox.Dock = System.Windows.Forms.DockStyle.Fill;
          this.textBox.Location = new System.Drawing.Point(0, 0);
          this.textBox.Multiline = true;
          this.textBox.Name = "textBox";
          this.textBox.Size = new System.Drawing.Size(162, 376);
          this.textBox.TabIndex = 0;
          // 
          // secondSplitContainer
          // 
          this.secondSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
          this.secondSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
          this.secondSplitContainer.Location = new System.Drawing.Point(0, 0);
          this.secondSplitContainer.Name = "secondSplitContainer";
          // 
          // secondSplitContainer.Panel1
          // 
          this.secondSplitContainer.Panel1.Controls.Add(this.dataGrid);
          // 
          // secondSplitContainer.Panel2
          // 
          this.secondSplitContainer.Panel2.Controls.Add(this.propertyGrid);
          this.secondSplitContainer.Size = new System.Drawing.Size(349, 376);
          this.secondSplitContainer.SplitterDistance = 183;
          this.secondSplitContainer.TabIndex = 0;
          // 
          // dataGrid
          // 
          this.dataGrid.AutoGenerateColumns = false;
          this.dataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
          this.dataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.firstDataGridViewTextBoxColumn,
            this.secondDataGridViewTextBoxColumn});
          this.dataGrid.DataSource = this.myDataSetBindingSource;
          this.dataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
          this.dataGrid.Location = new System.Drawing.Point(0, 0);
          this.dataGrid.Name = "dataGrid";
          this.dataGrid.Size = new System.Drawing.Size(183, 376);
          this.dataGrid.TabIndex = 0;
          // 
          // propertyGrid
          // 
          this.propertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
          this.propertyGrid.Location = new System.Drawing.Point(0, 0);
          this.propertyGrid.Name = "propertyGrid";
          this.propertyGrid.Size = new System.Drawing.Size(162, 376);
          this.propertyGrid.TabIndex = 0;
          // 
          // toolbar
          // 
          this.toolbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createObjectToolButton});
          this.toolbar.Location = new System.Drawing.Point(0, 24);
          this.toolbar.Name = "toolbar";
          this.toolbar.Size = new System.Drawing.Size(680, 25);
          this.toolbar.TabIndex = 4;
          this.toolbar.Text = "toolStrip1";
          // 
          // createObjectToolButton
          // 
          this.createObjectToolButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
          this.createObjectToolButton.Image = ((System.Drawing.Image)(resources.GetObject("createObjectToolButton.Image")));
          this.createObjectToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
          this.createObjectToolButton.Name = "createObjectToolButton";
          this.createObjectToolButton.Size = new System.Drawing.Size(79, 22);
          this.createObjectToolButton.Text = "Create Object";
          this.createObjectToolButton.Click += new System.EventHandler(this.createObjectMenuItem_Click);
          // 
          // centralPanel
          // 
          this.centralPanel.Controls.Add(this.mainSplitContainer);
          this.centralPanel.Dock = System.Windows.Forms.DockStyle.Fill;
          this.centralPanel.Location = new System.Drawing.Point(0, 49);
          this.centralPanel.Name = "centralPanel";
          this.centralPanel.Size = new System.Drawing.Size(680, 376);
          this.centralPanel.TabIndex = 5;
          // 
          // mainSplitContainer
          // 
          this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
          this.mainSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
          this.mainSplitContainer.Location = new System.Drawing.Point(0, 0);
          this.mainSplitContainer.Name = "mainSplitContainer";
          // 
          // mainSplitContainer.Panel1
          // 
          this.mainSplitContainer.Panel1.Controls.Add(this.firstSplitContainer);
          // 
          // mainSplitContainer.Panel2
          // 
          this.mainSplitContainer.Panel2.Controls.Add(this.secondSplitContainer);
          this.mainSplitContainer.Size = new System.Drawing.Size(680, 376);
          this.mainSplitContainer.SplitterDistance = 327;
          this.mainSplitContainer.TabIndex = 6;
          // 
          // openFileDialog
          // 
          this.openFileDialog.FileName = "openFileDialog1";
          // 
          // dataMenuItem
          // 
          this.dataMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadDataMenuItem,
            this.saveDataMenuItem});
          this.dataMenuItem.Name = "dataMenuItem";
          this.dataMenuItem.Size = new System.Drawing.Size(42, 20);
          this.dataMenuItem.Text = "Data";
          // 
          // loadDataMenuItem
          // 
          this.loadDataMenuItem.Name = "loadDataMenuItem";
          this.loadDataMenuItem.Size = new System.Drawing.Size(152, 22);
          this.loadDataMenuItem.Text = "Load Data";
          this.loadDataMenuItem.Click += new System.EventHandler(this.loadDataMenuItem_Click);
          // 
          // saveDataMenuItem
          // 
          this.saveDataMenuItem.Name = "saveDataMenuItem";
          this.saveDataMenuItem.Size = new System.Drawing.Size(152, 22);
          this.saveDataMenuItem.Text = "Save Data";
          this.saveDataMenuItem.Click += new System.EventHandler(this.saveDataMenuItem_Click);
          // 
          // firstDataGridViewTextBoxColumn
          // 
          this.firstDataGridViewTextBoxColumn.DataPropertyName = "First";
          this.firstDataGridViewTextBoxColumn.HeaderText = "First";
          this.firstDataGridViewTextBoxColumn.Name = "firstDataGridViewTextBoxColumn";
          // 
          // secondDataGridViewTextBoxColumn
          // 
          this.secondDataGridViewTextBoxColumn.DataPropertyName = "Second";
          this.secondDataGridViewTextBoxColumn.HeaderText = "Second";
          this.secondDataGridViewTextBoxColumn.Name = "secondDataGridViewTextBoxColumn";
          // 
          // myDataSetBindingSource
          // 
          this.myDataSetBindingSource.DataMember = "MyDataTable";
          this.myDataSetBindingSource.DataSource = this.myDataSet;
          // 
          // myDataSet
          // 
          this.myDataSet.DataSetName = "MyDataSet";
          this.myDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
          // 
          // MainForm
          // 
          this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
          this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
          this.ClientSize = new System.Drawing.Size(680, 447);
          this.Controls.Add(this.centralPanel);
          this.Controls.Add(this.toolbar);
          this.Controls.Add(this.menu);
          this.Controls.Add(this.status);
          this.MainMenuStrip = this.menu;
          this.Name = "MainForm";
          this.Text = "Form1";
          this.menu.ResumeLayout(false);
          this.menu.PerformLayout();
          this.status.ResumeLayout(false);
          this.status.PerformLayout();
          this.firstSplitContainer.Panel1.ResumeLayout(false);
          this.firstSplitContainer.Panel2.ResumeLayout(false);
          this.firstSplitContainer.Panel2.PerformLayout();
          this.firstSplitContainer.ResumeLayout(false);
          this.secondSplitContainer.Panel1.ResumeLayout(false);
          this.secondSplitContainer.Panel2.ResumeLayout(false);
          this.secondSplitContainer.ResumeLayout(false);
          ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).EndInit();
          this.toolbar.ResumeLayout(false);
          this.toolbar.PerformLayout();
          this.centralPanel.ResumeLayout(false);
          this.mainSplitContainer.Panel1.ResumeLayout(false);
          this.mainSplitContainer.Panel2.ResumeLayout(false);
          this.mainSplitContainer.ResumeLayout(false);
          ((System.ComponentModel.ISupportInitialize)(this.myDataSetBindingSource)).EndInit();
          ((System.ComponentModel.ISupportInitialize)(this.myDataSet)).EndInit();
          this.ResumeLayout(false);
          this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.MenuStrip menu;
      private System.Windows.Forms.StatusStrip status;
      private System.Windows.Forms.ToolStripMenuItem fileMenu;
      private System.Windows.Forms.ToolStripMenuItem quitMenuItem;
      private System.Windows.Forms.ToolStripMenuItem objectMenu;
      private System.Windows.Forms.ToolStripStatusLabel statusLabel;
      private System.Windows.Forms.SplitContainer firstSplitContainer;
      private System.Windows.Forms.SplitContainer secondSplitContainer;
      private System.Windows.Forms.TreeView treeView;
      private System.Windows.Forms.PropertyGrid propertyGrid;
      private System.Windows.Forms.ToolStrip toolbar;
      private System.Windows.Forms.ToolStripMenuItem openXmlMenuItem;
      private System.Windows.Forms.Panel centralPanel;
      private System.Windows.Forms.SplitContainer mainSplitContainer;
      private System.Windows.Forms.OpenFileDialog openFileDialog;
      private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
      private System.Windows.Forms.ToolStripMenuItem saveXmlMenuItem;
      private System.Windows.Forms.SaveFileDialog saveFileDialog;
      private System.Windows.Forms.TextBox textBox;
      private System.Windows.Forms.DataGridView dataGrid;
      private System.Windows.Forms.ToolStripMenuItem createObjectMenuItem;
      private System.Windows.Forms.ToolStripButton createObjectToolButton;
      private System.Windows.Forms.ToolStripMenuItem toTextMenuItem;
      private System.Windows.Forms.ToolStripMenuItem fromTextMenuItem;
      private System.Windows.Forms.ToolStripMenuItem objectToTextMenuItem;
      private System.Windows.Forms.ToolStripMenuItem objectFromTextMenuItem;
      private System.Windows.Forms.ToolStripMenuItem loadObjectMenuItem;
      private System.Windows.Forms.ToolStripMenuItem saveObjectMenuItem;
      private System.Windows.Forms.BindingSource myDataSetBindingSource;
      private MyDataSet myDataSet;
      private System.Windows.Forms.DataGridViewTextBoxColumn firstDataGridViewTextBoxColumn;
      private System.Windows.Forms.DataGridViewTextBoxColumn secondDataGridViewTextBoxColumn;
      private System.Windows.Forms.ToolStripMenuItem dataMenuItem;
      private System.Windows.Forms.ToolStripMenuItem loadDataMenuItem;
      private System.Windows.Forms.ToolStripMenuItem saveDataMenuItem;
   }
}

