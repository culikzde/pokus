﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace Drawing
{
    public partial class MainWindow : Form
    {
        private Pen pen;
        private Brush brush;
        private Color basic1;
        private Color basic2;
        private int cnt = 1;

        public MainWindow()
        {
            InitializeComponent();
            pictureBox_SizeChanged(null, null);
            comboBox.SelectedIndex = 0;

            Panel p2 = newColorPanel(Color.Red);
            Panel p3 = newColorPanel(Color.Green);

            pen = new Pen (colorPanel.BackColor);
            basic1 = p2.BackColor;
            basic2 = basic1;
            updateBrush ();
        }

        private Panel newColorPanel (Color c)
        {
            Panel p = new Panel();
            p.Parent = toolPanel;
            cnt ++;
            p.Left = colorPanel.Left + (cnt - 1) * (colorPanel.Width + colorPanel.Left);
            p.Top = colorPanel.Top;
            p.Width = colorPanel.Width;
            p.Height = colorPanel.Height;
            p.BackColor = c;
            p.MouseDown += this.colorPanel_MouseDown;
            return p;
        }

        private void newColorMenu_Click(object sender, EventArgs e)
        {
            newColorPanel (Color.Yellow);
        }

        private void xcoord_ValueChanged(object sender, EventArgs e)
        {
            updateBrush();
        }

        private void updateBrush()
        {
            int xc = (int) xcoord.Value;
            int yc = (int) ycoord.Value;

            // using System.Drawing.Drawing2D;
            brush = new LinearGradientBrush
                        (new Point(0, 0),
                         new Point(xc, yc),
                         basic1,
                         basic2);
        }

        private void colorPanel_MouseDown(object sender, MouseEventArgs e)
        {
            Panel panel = (Panel) sender;
            Color color = panel.BackColor;
            if (e.Button == MouseButtons.Left)
            {
                pen = new Pen(color);
            }
            else if (e.Button == MouseButtons.Right)
            {
                if (Control.ModifierKeys == Keys.Shift)
                    basic1 = color;
                else if (Control.ModifierKeys == Keys.Control)
                    basic2 = color;
                else
                    basic1 = basic2 = color;

                updateBrush();
            }
            else if (e.Button == MouseButtons.Middle)
            {
                colorDialog.Color = color;
                if (colorDialog.ShowDialog() == DialogResult.OK)
                {
                    color = colorDialog.Color;
                    panel.BackColor = color;

                    pen = new Pen(color);
                }
            }
        }


        private void pictureBox_SizeChanged (object sender, EventArgs e)
        {
            int w = pictureBox.Width;
            int h = pictureBox.Height;

            if (pictureBox.Image != null)
            {
                if (w < pictureBox.Image.Width)
                    w = pictureBox.Image.Width;
                if (h < pictureBox.Image.Height)
                    h = pictureBox.Image.Height;
            }

            Bitmap bitmap = new Bitmap (w, h);

            Graphics g = Graphics.FromImage (bitmap);

            Brush b = new SolidBrush (Color.White);
            g.FillRectangle (b, 0, 0, w, h);

            if (pictureBox.Image != null)
               g.DrawImage (pictureBox.Image, 0, 0);

            pictureBox.Image = bitmap;
        }

        private int X0, Y0;
        private bool down = false;
        private Bitmap save = null;

        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
           X0 = e.X;
           Y0 = e.Y;
           down = true;
           save = new Bitmap (pictureBox.Image);
        }

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (down)
            {
                Graphics g = Graphics.FromImage(pictureBox.Image);
                g.DrawImage(save, 0, 0);
                Pen p = pen;
                p.Width = 1;
                
                int X1 = X0;
                int Y1 = Y0;
                int X2 = e.X;
                int Y2 = e.Y;
                if (X1 > X2) { int t = X1; X1 = X2; X2 = t; }
                if (Y1 > Y2) { int t = Y1; Y1 = Y2; Y2 = t; }

                int inx = comboBox.SelectedIndex;

                if (inx == 0)
                    g.DrawLine(p, X1, Y1, X2, Y2);
                else if (inx == 1)
                    g.DrawRectangle (p, X1, Y1, X2-X1, Y2-Y1);
                else if (inx == 2)
                   g.DrawEllipse (p, X1, Y1, X2-X1, Y2-Y1);
                
                pictureBox.Invalidate();
            }
        }

        private void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (down)
            {
                down = false;
                int inx = comboBox.SelectedIndex;

                Graphics g = Graphics.FromImage(pictureBox.Image);
                if (save != null)
                    g.DrawImage(save, 0, 0);

                int X1 = X0;
                int Y1 = Y0;
                int X2 = e.X;
                int Y2 = e.Y;
                if (X1 > X2) { int t = X1; X1 = X2; X2 = t; }
                if (Y1 > Y2) { int t = Y1; Y1 = Y2; Y2 = t; }

                if (inx == 0)
                    g.DrawLine (pen, X1, Y1, X2, Y2);
                else if (inx == 1)
                {
                    g.FillRectangle (brush, X1, Y1, X2 - X1, Y2 - Y1);
                    g.DrawRectangle (pen, X1, Y1, X2 - X1, Y2 - Y1);
                }
                else if (inx == 2)
                {
                    g.FillEllipse (brush, X1, Y1, X2 - X1, Y2 - Y1);
                    g.DrawEllipse (pen, X1, Y1, X2 - X1, Y2 - Y1);
                }

                pictureBox.Invalidate();
            }
        }

        private void quitMenu_Click(object sender, EventArgs e)
        {
            Close ();
        }

        private void openMenu_Click(object sender, EventArgs e)
        {
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                pictureBox.Image = new Bitmap (openDialog.FileName);
                pictureBox_SizeChanged (null, null);
            }
        }

        private void saveMenu_Click(object sender, EventArgs e)
        {
            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                // using System.Drawing.Imaging;
                // saveDialog.Filter : Bitmap|*.bmp|Jpeg|*.jpg
                if (saveDialog.FilterIndex == 1)
                    pictureBox.Image.Save (saveDialog.FileName, ImageFormat.Bmp);
                else if (saveDialog.FilterIndex == 2)
                    pictureBox.Image.Save (saveDialog.FileName, ImageFormat.Jpeg);
            }
        }


    } // end of class
} // end of namespace
