﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Drawing.Design;

using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis;
using System.Dynamic;

namespace designer
{
    public partial class Window : Form
    {
        private static Window instance = null;
        public static Window getInstance () { return instance; }

        public Window ()
        {
            instance = this;
            InitializeComponent ();

            addComponent (typeof (Button));
            addComponent (typeof (TreeView));
            addComponent (typeof (MyControl), typeof (Panel));
            addComponent (typeof (DataGrid));

            addColor ("red");
            addColor ("cornflowerblue");
            addColor ("lime");
            addColor ("yellow");

            area.AllowDrop = true;
        }

        public void addComponent (Type type, Type image_type = null)
        {
            // using System.Drawing.Design;
            ToolboxItem item = new ToolboxItem (type);

            ToolStripButton btn = new ToolStripButton ();
            btn.Image = item.Bitmap;
            btn.ToolTipText = item.DisplayName;
            btn.Tag = item;
            btn.MouseDown += component_MouseDown;

            if (image_type != null)
                btn.Image = new ToolboxItem (image_type).Bitmap;

            componentPalette.Items.Add (btn);
        }

        public void addColor (Color color)
        {
            const int size = 32;
            Bitmap img = new Bitmap (size, size);
            Graphics g = Graphics.FromImage (img);
            g.FillEllipse (new SolidBrush (color), 1, 1, size-2, size-2);

            ToolStripButton btn = new ToolStripButton ();
            btn.Image = img;
            btn.ToolTipText = color.Name;
            btn.Tag = color;
            btn.MouseDown += color_MouseDown;
            colorPalette.Items.Add (btn);
        }

        public void addColor (string name)
        {
            addColor (Color.FromName (name));
        }

        ToolboxItem drag_item = null;
        Color drag_color;

        private void component_MouseDown (object sender, EventArgs e)
        {
            drag_item = (sender as ToolStripButton).Tag as ToolboxItem;
            DoDragDrop ("component", DragDropEffects.Copy);

            /*
            IComponent [] comp_list = item.CreateComponents ();
            Control comp = comp_list[0] as Control;
            area.Controls.Add (comp);
            */
        }

        private void color_MouseDown (object sender, EventArgs e)
        {
            drag_color = (Color) ((sender as ToolStripButton).Tag);
            DoDragDrop ("color", DragDropEffects.Copy);
        }

        private void area_DragEnter (object sender, DragEventArgs e)
        {
            string fmt = e.Data.GetData ("Text") as string;
            if (fmt == "component" || fmt == "color")
                e.Effect = e.AllowedEffect;
        }

        private void area_DragDrop (object sender, DragEventArgs e)
        {
            string fmt = e.Data.GetData ("Text") as string;
            if (fmt == "component" )
            {
                IComponent[] comp_list = drag_item.CreateComponents ();
                Control comp = comp_list[0] as Control;
                Point p = area.PointToClient (new Point (e.X, e.Y));
                Size s = comp.Size;
                comp.Location = p;
                comp.Size = s;
                area.Controls.Add (comp);
                statusLabel.Text = "X=" + e.X + ", Y=" + e.Y;
                new Adapter (comp);
                displayTree ();
            }
            if (fmt == "color")
            {
                area.BackColor = drag_color;
            }
        }

        private void displayTree ()
        {
            tree.Nodes.Clear ();
            displayBranch (tree.Nodes, area.Controls);
        }

        private void displayBranch (TreeNodeCollection target,
                                    Control.ControlCollection ctrls)
        {
            foreach (Control c in ctrls)
            {
                TreeNode n = new TreeNode ();
                n.Text = c.GetType ().Name;
                n.Tag = c;
                target.Add (n);

                displayBranch (n.Nodes, c.Controls);
            }
        }

        private void tree_NodeMouseClick (object sender, TreeNodeMouseClickEventArgs e)
        {
            propGrid.SelectedObject = e.Node.Tag;
        }

        private async void runMenu_ClickAsync (object sender, EventArgs e)
        {
            // dynamic obj = new ExpandoObject ();
            // obj.Abc = "Klm";
            // propGrid.SelectedObject = obj;

            // propGrid.SelectedObject = new { Abc = "abc" };


            /*
               1 + 2
               X + Y
               Win.Width
               Win.Text
               Win.Text = "Abc";
             */ 

            string source = input.Text;
            var variables = new Variables { X = 1, Y = 2 , Win = this };
            // object result = await CSharpScript.EvaluateAsync (source, globals : variables);
            var state = await CSharpScript.RunAsync (source, globals : variables);
            var result = state.ReturnValue;
            var compilation = state.Script.GetCompilation ();

            output.AppendText (source + " -> " + result + "\n");
            foreach (var v in state.Variables)
                output.AppendText ("   " + v.Name + " = " + v.Value + "\n");
            foreach (SyntaxTree t in compilation.SyntaxTrees)
                ;
            InOutTabControl.SelectedTab = outputPage;
        }

    }

    // Tools > Options > Environment > Fonts and Colors
    // Tools > Options > Text Editor > C# > CodeStyle > Formatting > Spacing

    // using Microsoft.CodeAnalysis.CSharp.Scripting;
    // https://github.com/dotnet/roslyn/wiki/Scripting-API-Samples

    // Tools > NuGet Package Manager > Package Manager Console
    // Install-Package Microsoft.CodeAnalysis.CSharp.Scripting
}
