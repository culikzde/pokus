﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace designer
{
    public partial class Adapter : UserControl
    {
        private Control comp;

        public Adapter (Control c)
        {
            InitializeComponent ();
            comp = c;
            comp.MouseDown += Adapter_MouseDown;
            comp.MouseMove += Adapter_MouseMove;
            comp.MouseUp += Adapter_MouseUp;
            comp.Tag = this;
        }

        bool click = false;
        int X0, Y0;

        private void Adapter_MouseDown (object sender, MouseEventArgs e)
        {
            click = true;
            X0 = e.X;
            Y0 = e.Y;
            if (e.Button == MouseButtons.Middle)
                Window.getInstance ().propGrid.SelectedObject = comp;
        }

        private void Adapter_MouseMove (object sender, MouseEventArgs e)
        {
            if (click)
            {
                if (e.Button == MouseButtons.Left)
                {
                    comp.Left += e.X - X0;
                    comp.Top += e.Y - Y0;
                }
                if (e.Button == MouseButtons.Right)
                {
                    comp.Width += e.X - X0;
                    comp.Height += e.Y - Y0;
                    X0 = e.X;
                    Y0 = e.Y;
                }

            }
        }

        private void Adapter_MouseUp (object sender, MouseEventArgs e)
        {
            click = false;
        }

        private void Adapter_DragEnter (object sender, DragEventArgs e)
        {

        }

        private void Adapter_DragDrop (object sender, DragEventArgs e)
        {

        }
    }
}
