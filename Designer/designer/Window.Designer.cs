﻿namespace designer
{
    partial class Window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Window));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.tree = new System.Windows.Forms.TreeView();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.area = new System.Windows.Forms.Panel();
            this.palette = new System.Windows.Forms.TabControl();
            this.ComponentPage = new System.Windows.Forms.TabPage();
            this.componentPalette = new System.Windows.Forms.ToolStrip();
            this.colorPage = new System.Windows.Forms.TabPage();
            this.colorPalette = new System.Windows.Forms.ToolStrip();
            this.propGrid = new System.Windows.Forms.PropertyGrid();
            this.InOutTabControl = new System.Windows.Forms.TabControl();
            this.inputPage = new System.Windows.Forms.TabPage();
            this.input = new System.Windows.Forms.TextBox();
            this.outputPage = new System.Windows.Forms.TabPage();
            this.output = new System.Windows.Forms.TextBox();
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.openMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.saveMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.quitMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.projectMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.runMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolBar = new System.Windows.Forms.ToolStrip();
            this.runButton = new System.Windows.Forms.ToolStripButton();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.openDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveDialog = new System.Windows.Forms.SaveFileDialog();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.palette.SuspendLayout();
            this.ComponentPage.SuspendLayout();
            this.colorPage.SuspendLayout();
            this.InOutTabControl.SuspendLayout();
            this.inputPage.SuspendLayout();
            this.outputPage.SuspendLayout();
            this.mainMenu.SuspendLayout();
            this.toolBar.SuspendLayout();
            this.statusBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 49);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.InOutTabControl);
            this.splitContainer1.Size = new System.Drawing.Size(1064, 523);
            this.splitContainer1.SplitterDistance = 358;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.tree);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(1064, 358);
            this.splitContainer2.SplitterDistance = 280;
            this.splitContainer2.TabIndex = 0;
            // 
            // tree
            // 
            this.tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tree.Location = new System.Drawing.Point(0, 0);
            this.tree.Name = "tree";
            this.tree.Size = new System.Drawing.Size(280, 358);
            this.tree.TabIndex = 0;
            this.tree.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tree_NodeMouseClick);
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.area);
            this.splitContainer3.Panel1.Controls.Add(this.palette);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.propGrid);
            this.splitContainer3.Size = new System.Drawing.Size(780, 358);
            this.splitContainer3.SplitterDistance = 416;
            this.splitContainer3.TabIndex = 0;
            // 
            // area
            // 
            this.area.Dock = System.Windows.Forms.DockStyle.Fill;
            this.area.Location = new System.Drawing.Point(0, 59);
            this.area.Name = "area";
            this.area.Size = new System.Drawing.Size(416, 299);
            this.area.TabIndex = 1;
            this.area.DragDrop += new System.Windows.Forms.DragEventHandler(this.area_DragDrop);
            this.area.DragEnter += new System.Windows.Forms.DragEventHandler(this.area_DragEnter);
            // 
            // palette
            // 
            this.palette.Controls.Add(this.ComponentPage);
            this.palette.Controls.Add(this.colorPage);
            this.palette.Dock = System.Windows.Forms.DockStyle.Top;
            this.palette.Location = new System.Drawing.Point(0, 0);
            this.palette.Name = "palette";
            this.palette.SelectedIndex = 0;
            this.palette.Size = new System.Drawing.Size(416, 59);
            this.palette.TabIndex = 0;
            // 
            // ComponentPage
            // 
            this.ComponentPage.Controls.Add(this.componentPalette);
            this.ComponentPage.Location = new System.Drawing.Point(4, 22);
            this.ComponentPage.Name = "ComponentPage";
            this.ComponentPage.Padding = new System.Windows.Forms.Padding(3);
            this.ComponentPage.Size = new System.Drawing.Size(408, 33);
            this.ComponentPage.TabIndex = 0;
            this.ComponentPage.Text = "Components";
            this.ComponentPage.UseVisualStyleBackColor = true;
            // 
            // componentPalette
            // 
            this.componentPalette.Dock = System.Windows.Forms.DockStyle.Fill;
            this.componentPalette.Location = new System.Drawing.Point(3, 3);
            this.componentPalette.Name = "componentPalette";
            this.componentPalette.Size = new System.Drawing.Size(402, 27);
            this.componentPalette.TabIndex = 0;
            this.componentPalette.Text = "toolStrip1";
            // 
            // colorPage
            // 
            this.colorPage.Controls.Add(this.colorPalette);
            this.colorPage.Location = new System.Drawing.Point(4, 22);
            this.colorPage.Name = "colorPage";
            this.colorPage.Padding = new System.Windows.Forms.Padding(3);
            this.colorPage.Size = new System.Drawing.Size(408, 33);
            this.colorPage.TabIndex = 1;
            this.colorPage.Text = "Colors";
            this.colorPage.UseVisualStyleBackColor = true;
            // 
            // colorPalette
            // 
            this.colorPalette.Dock = System.Windows.Forms.DockStyle.Fill;
            this.colorPalette.Location = new System.Drawing.Point(3, 3);
            this.colorPalette.Name = "colorPalette";
            this.colorPalette.Size = new System.Drawing.Size(402, 27);
            this.colorPalette.TabIndex = 0;
            this.colorPalette.Text = "toolStrip2";
            // 
            // propGrid
            // 
            this.propGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propGrid.LineColor = System.Drawing.SystemColors.ControlDark;
            this.propGrid.Location = new System.Drawing.Point(0, 0);
            this.propGrid.Name = "propGrid";
            this.propGrid.Size = new System.Drawing.Size(360, 358);
            this.propGrid.TabIndex = 0;
            // 
            // InOutTabControl
            // 
            this.InOutTabControl.Controls.Add(this.inputPage);
            this.InOutTabControl.Controls.Add(this.outputPage);
            this.InOutTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InOutTabControl.Location = new System.Drawing.Point(0, 0);
            this.InOutTabControl.Name = "InOutTabControl";
            this.InOutTabControl.SelectedIndex = 0;
            this.InOutTabControl.Size = new System.Drawing.Size(1064, 161);
            this.InOutTabControl.TabIndex = 0;
            // 
            // inputPage
            // 
            this.inputPage.Controls.Add(this.input);
            this.inputPage.Location = new System.Drawing.Point(4, 22);
            this.inputPage.Name = "inputPage";
            this.inputPage.Padding = new System.Windows.Forms.Padding(3);
            this.inputPage.Size = new System.Drawing.Size(1056, 135);
            this.inputPage.TabIndex = 0;
            this.inputPage.Text = "Input";
            this.inputPage.UseVisualStyleBackColor = true;
            // 
            // input
            // 
            this.input.Dock = System.Windows.Forms.DockStyle.Fill;
            this.input.Location = new System.Drawing.Point(3, 3);
            this.input.Multiline = true;
            this.input.Name = "input";
            this.input.Size = new System.Drawing.Size(1050, 129);
            this.input.TabIndex = 0;
            this.input.Text = "1 + 2";
            // 
            // outputPage
            // 
            this.outputPage.Controls.Add(this.output);
            this.outputPage.Location = new System.Drawing.Point(4, 22);
            this.outputPage.Name = "outputPage";
            this.outputPage.Padding = new System.Windows.Forms.Padding(3);
            this.outputPage.Size = new System.Drawing.Size(1056, 135);
            this.outputPage.TabIndex = 1;
            this.outputPage.Text = "Output";
            this.outputPage.UseVisualStyleBackColor = true;
            // 
            // output
            // 
            this.output.Dock = System.Windows.Forms.DockStyle.Fill;
            this.output.Location = new System.Drawing.Point(3, 3);
            this.output.Multiline = true;
            this.output.Name = "output";
            this.output.Size = new System.Drawing.Size(1050, 129);
            this.output.TabIndex = 1;
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.editMenu,
            this.projectMenu});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(1064, 24);
            this.mainMenu.TabIndex = 1;
            this.mainMenu.Text = "menuStrip1";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openMenu,
            this.saveMenu,
            this.quitMenu});
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(37, 20);
            this.fileMenu.Text = "File";
            // 
            // openMenu
            // 
            this.openMenu.Name = "openMenu";
            this.openMenu.Size = new System.Drawing.Size(103, 22);
            this.openMenu.Text = "Open";
            // 
            // saveMenu
            // 
            this.saveMenu.Name = "saveMenu";
            this.saveMenu.Size = new System.Drawing.Size(103, 22);
            this.saveMenu.Text = "Save";
            // 
            // quitMenu
            // 
            this.quitMenu.Name = "quitMenu";
            this.quitMenu.Size = new System.Drawing.Size(103, 22);
            this.quitMenu.Text = "Quit";
            // 
            // editMenu
            // 
            this.editMenu.Name = "editMenu";
            this.editMenu.Size = new System.Drawing.Size(39, 20);
            this.editMenu.Text = "&Edit";
            // 
            // projectMenu
            // 
            this.projectMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.runMenu});
            this.projectMenu.Name = "projectMenu";
            this.projectMenu.Size = new System.Drawing.Size(56, 20);
            this.projectMenu.Text = "Project";
            // 
            // runMenu
            // 
            this.runMenu.Name = "runMenu";
            this.runMenu.ShortcutKeys = System.Windows.Forms.Keys.F9;
            this.runMenu.Size = new System.Drawing.Size(152, 22);
            this.runMenu.Text = "Run";
            this.runMenu.Click += new System.EventHandler(this.runMenu_ClickAsync);
            // 
            // toolBar
            // 
            this.toolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.runButton});
            this.toolBar.Location = new System.Drawing.Point(0, 24);
            this.toolBar.Name = "toolBar";
            this.toolBar.Size = new System.Drawing.Size(1064, 25);
            this.toolBar.TabIndex = 2;
            this.toolBar.Text = "toolStrip1";
            // 
            // runButton
            // 
            this.runButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.runButton.Image = ((System.Drawing.Image)(resources.GetObject("runButton.Image")));
            this.runButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(32, 22);
            this.runButton.Text = "Run";
            this.runButton.Click += new System.EventHandler(this.runMenu_ClickAsync);
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusBar.Location = new System.Drawing.Point(0, 572);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(1064, 22);
            this.statusBar.TabIndex = 0;
            this.statusBar.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // openDialog
            // 
            this.openDialog.FileName = "openFileDialog1";
            // 
            // imageList
            // 
            this.imageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1064, 594);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolBar);
            this.Controls.Add(this.mainMenu);
            this.Controls.Add(this.statusBar);
            this.MainMenuStrip = this.mainMenu;
            this.Name = "Form1";
            this.Text = "Form1";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.palette.ResumeLayout(false);
            this.ComponentPage.ResumeLayout(false);
            this.ComponentPage.PerformLayout();
            this.colorPage.ResumeLayout(false);
            this.colorPage.PerformLayout();
            this.InOutTabControl.ResumeLayout(false);
            this.inputPage.ResumeLayout(false);
            this.inputPage.PerformLayout();
            this.outputPage.ResumeLayout(false);
            this.outputPage.PerformLayout();
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.toolBar.ResumeLayout(false);
            this.toolBar.PerformLayout();
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TreeView tree;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Panel area;
        private System.Windows.Forms.TabControl palette;
        private System.Windows.Forms.TabPage ComponentPage;
        private System.Windows.Forms.ToolStrip componentPalette;
        private System.Windows.Forms.TabPage colorPage;
        private System.Windows.Forms.ToolStrip colorPalette;
        private System.Windows.Forms.TabControl InOutTabControl;
        private System.Windows.Forms.TabPage inputPage;
        private System.Windows.Forms.TextBox input;
        private System.Windows.Forms.TabPage outputPage;
        private System.Windows.Forms.TextBox output;
        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStrip toolBar;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.OpenFileDialog openDialog;
        private System.Windows.Forms.SaveFileDialog saveDialog;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem openMenu;
        private System.Windows.Forms.ToolStripMenuItem saveMenu;
        private System.Windows.Forms.ToolStripMenuItem quitMenu;
        private System.Windows.Forms.ToolStripMenuItem editMenu;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        public System.Windows.Forms.PropertyGrid propGrid;
        private System.Windows.Forms.ToolStripMenuItem projectMenu;
        private System.Windows.Forms.ToolStripMenuItem runMenu;
        private System.Windows.Forms.ToolStripButton runButton;
    }
}

