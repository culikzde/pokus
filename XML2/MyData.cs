﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XML2
{
    public class MyPoint
    {
        private int x;

        public int X
        {
            get { return x; }
            set { x = value; }
        }

        private int y;

        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }
    }

    public class MyData
    {
        private int cislo;

        public int Cislo
        {
            get { return cislo; }
            set { cislo = value; }
        }

        private string text;

        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        private List<MyPoint> body = new List<MyPoint>();

        public List<MyPoint> Body
        {
            get { return body; }
            set { body = value; }
        }
    }
}
