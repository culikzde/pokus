﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace XML2
{
    public partial class Form1 : Form
    {
        private MyData data;

        public Form1()
        {
            InitializeComponent();
            data = new MyData ();
            propertyGrid1.SelectedObject = data;
            data.Cislo = 7;
            data.Text = "abc";
            for (int i = 1; i <= 3; i++)
            {
                MyPoint p = new MyPoint();
                p.X = i;
                p.Y = i;
                p.Name = "puntik cislo " + i;
                data.Body.Add (p);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // using System.IO;
            StreamWriter stream =
                new StreamWriter ("c:\\work\\zc\\data3.xml");
            
            // using System.Xml.Serialization;
            XmlSerializer writer = 
                new XmlSerializer (typeof (MyData));
            
            writer.Serialize (stream, data);

            stream.Close ();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // using System.IO;
            StreamReader stream =
                new StreamReader("c:\\work\\zc\\data3.xml");

            // using System.Xml.Serialization;
            XmlSerializer writer =
                new XmlSerializer(typeof(MyData));

            object obj = writer.Deserialize (stream);
            data = obj as MyData;
            propertyGrid1.SelectedObject = data;

            stream.Close();

        }

        private void show(TreeNodeCollection target,
                           XmlNode node)
        {
            TreeNode item = new TreeNode ();
            target.Add (item);
            if (node is XmlElement)
            {
                XmlElement elem = node as XmlElement;
                item.Text = elem.Name;
                item.ForeColor = Color.Blue;
                foreach (XmlAttribute a in elem.Attributes)
                {
                    TreeNode t = new TreeNode();
                    t.Text = a.Name + "=" + a.Value;
                    t.ForeColor = Color.Lime;
                    item.Nodes.Add(t);
                }
                foreach (XmlNode n in elem.ChildNodes)
                    show (item.Nodes, n);
            }
            else if (node is XmlText)
            {
                XmlText text = node as XmlText;
                item.Text = text.Value;
                item.ForeColor = Color.Orange;
            }
            else
            {
                item.Text = "" + node;
                item.ForeColor = Color.Red;
            }
        }

        private void read(String fileName)
        {
            // using System.Xml;
            XmlDocument doc = new XmlDocument ();
            // doc.Load(fileName);
            doc.LoadXml("<knihovna/>");
            XmlElement elem = doc.DocumentElement;

            XmlElement k = doc.CreateElement("kniha");
            elem.AppendChild(k);

            XmlElement s = doc.CreateElement("strana");
            s.SetAttribute("cislo", "");
            k.AppendChild(s);

            show (treeView1.Nodes, elem);
            doc.Save ("c:\\work\\zc\\data2.xml");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            /*
            if (openFileDialog1.ShowDialog()
                   == DialogResult.OK)
            {
                read (openFileDialog1.FileName);
            }
            */
            read ("C:\\Work\\ZC\\data.xml");
        }


    }
}
