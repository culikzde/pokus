﻿namespace FileTree
{
   partial class TreeForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose (bool disposing)
      {
         if ( disposing && (components != null) )
         {
            components.Dispose ();
         }
         base.Dispose (disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent ()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager (typeof (TreeForm));
         this.toolBar = new System.Windows.Forms.ToolStrip ();
         this.runButton = new System.Windows.Forms.ToolStripButton ();
         this.menuBar = new System.Windows.Forms.MenuStrip ();
         this.fileMenu = new System.Windows.Forms.ToolStripMenuItem ();
         this.runMenuItem = new System.Windows.Forms.ToolStripMenuItem ();
         this.statusBar = new System.Windows.Forms.StatusStrip ();
         this.statusField = new System.Windows.Forms.ToolStripStatusLabel ();
         this.firstSplitter = new System.Windows.Forms.SplitContainer ();
         this.treeView = new System.Windows.Forms.TreeView ();
         this.secondSplitter = new System.Windows.Forms.SplitContainer ();
         this.listView = new System.Windows.Forms.ListView ();
         this.fileNameColumn = new System.Windows.Forms.ColumnHeader ();
         this.fileSizeColumn = new System.Windows.Forms.ColumnHeader ();
         this.fileDateColumn = new System.Windows.Forms.ColumnHeader ();
         this.tabControl = new System.Windows.Forms.TabControl ();
         this.textTab = new System.Windows.Forms.TabPage ();
         this.richTextBox = new System.Windows.Forms.RichTextBox ();
         this.pictureTab = new System.Windows.Forms.TabPage ();
         this.pictureBox = new System.Windows.Forms.PictureBox ();
         this.htmlTab = new System.Windows.Forms.TabPage ();
         this.webBrowser = new System.Windows.Forms.WebBrowser ();
         this.dataTab = new System.Windows.Forms.TabPage ();
         this.dataGridView = new System.Windows.Forms.DataGridView ();
         this.xmlTab = new System.Windows.Forms.TabPage ();
         this.treeBox = new System.Windows.Forms.TreeView ();
         this.toolBar.SuspendLayout ();
         this.menuBar.SuspendLayout ();
         this.statusBar.SuspendLayout ();
         this.firstSplitter.Panel1.SuspendLayout ();
         this.firstSplitter.Panel2.SuspendLayout ();
         this.firstSplitter.SuspendLayout ();
         this.secondSplitter.Panel1.SuspendLayout ();
         this.secondSplitter.Panel2.SuspendLayout ();
         this.secondSplitter.SuspendLayout ();
         this.tabControl.SuspendLayout ();
         this.textTab.SuspendLayout ();
         this.pictureTab.SuspendLayout ();
         ((System.ComponentModel.ISupportInitialize) (this.pictureBox)).BeginInit ();
         this.htmlTab.SuspendLayout ();
         this.dataTab.SuspendLayout ();
         ((System.ComponentModel.ISupportInitialize) (this.dataGridView)).BeginInit ();
         this.xmlTab.SuspendLayout ();
         this.SuspendLayout ();
         // 
         // toolBar
         // 
         this.toolBar.Items.AddRange (new System.Windows.Forms.ToolStripItem [] {
            this.runButton});
         this.toolBar.Location = new System.Drawing.Point (0, 24);
         this.toolBar.Name = "toolBar";
         this.toolBar.Size = new System.Drawing.Size (1047, 25);
         this.toolBar.TabIndex = 1;
         this.toolBar.Text = "toolStrip1";
         // 
         // runButton
         // 
         this.runButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
         this.runButton.Image = ((System.Drawing.Image) (resources.GetObject ("runButton.Image")));
         this.runButton.ImageTransparentColor = System.Drawing.Color.Magenta;
         this.runButton.Name = "runButton";
         this.runButton.Size = new System.Drawing.Size (30, 22);
         this.runButton.Text = "Run";
         this.runButton.Click += new System.EventHandler (this.runButton_Click);
         // 
         // menuBar
         // 
         this.menuBar.Items.AddRange (new System.Windows.Forms.ToolStripItem [] {
            this.fileMenu});
         this.menuBar.Location = new System.Drawing.Point (0, 0);
         this.menuBar.Name = "menuBar";
         this.menuBar.Size = new System.Drawing.Size (1047, 24);
         this.menuBar.TabIndex = 2;
         this.menuBar.Text = "menuStrip1";
         // 
         // fileMenu
         // 
         this.fileMenu.DropDownItems.AddRange (new System.Windows.Forms.ToolStripItem [] {
            this.runMenuItem});
         this.fileMenu.Name = "fileMenu";
         this.fileMenu.Size = new System.Drawing.Size (35, 20);
         this.fileMenu.Text = "File";
         // 
         // runMenuItem
         // 
         this.runMenuItem.Name = "runMenuItem";
         this.runMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys) ((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
         this.runMenuItem.Size = new System.Drawing.Size (143, 22);
         this.runMenuItem.Text = "Run";
         this.runMenuItem.Click += new System.EventHandler (this.runButton_Click);
         // 
         // statusBar
         // 
         this.statusBar.Items.AddRange (new System.Windows.Forms.ToolStripItem [] {
            this.statusField});
         this.statusBar.Location = new System.Drawing.Point (0, 468);
         this.statusBar.Name = "statusBar";
         this.statusBar.Size = new System.Drawing.Size (1047, 22);
         this.statusBar.TabIndex = 3;
         this.statusBar.Text = "statusStrip1";
         // 
         // statusField
         // 
         this.statusField.Name = "statusField";
         this.statusField.Size = new System.Drawing.Size (109, 17);
         this.statusField.Text = "toolStripStatusLabel1";
         // 
         // firstSplitter
         // 
         this.firstSplitter.Dock = System.Windows.Forms.DockStyle.Fill;
         this.firstSplitter.Location = new System.Drawing.Point (0, 49);
         this.firstSplitter.Name = "firstSplitter";
         // 
         // firstSplitter.Panel1
         // 
         this.firstSplitter.Panel1.Controls.Add (this.treeView);
         // 
         // firstSplitter.Panel2
         // 
         this.firstSplitter.Panel2.Controls.Add (this.secondSplitter);
         this.firstSplitter.Size = new System.Drawing.Size (1047, 419);
         this.firstSplitter.SplitterDistance = 348;
         this.firstSplitter.TabIndex = 4;
         // 
         // treeView
         // 
         this.treeView.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.treeView.Location = new System.Drawing.Point (0, 0);
         this.treeView.Name = "treeView";
         this.treeView.Size = new System.Drawing.Size (345, 419);
         this.treeView.TabIndex = 0;
         this.treeView.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler (this.treeView_BeforeExpand);
         this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler (this.treeView_AfterSelect);
         this.treeView.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler (this.treeView_BeforeSelect);
         this.treeView.AfterExpand += new System.Windows.Forms.TreeViewEventHandler (this.treeView_AfterExpand);
         // 
         // secondSplitter
         // 
         this.secondSplitter.Dock = System.Windows.Forms.DockStyle.Fill;
         this.secondSplitter.Location = new System.Drawing.Point (0, 0);
         this.secondSplitter.Name = "secondSplitter";
         // 
         // secondSplitter.Panel1
         // 
         this.secondSplitter.Panel1.Controls.Add (this.listView);
         // 
         // secondSplitter.Panel2
         // 
         this.secondSplitter.Panel2.Controls.Add (this.tabControl);
         this.secondSplitter.Size = new System.Drawing.Size (695, 419);
         this.secondSplitter.SplitterDistance = 231;
         this.secondSplitter.TabIndex = 0;
         // 
         // listView
         // 
         this.listView.Activation = System.Windows.Forms.ItemActivation.OneClick;
         this.listView.AllowColumnReorder = true;
         this.listView.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.listView.Columns.AddRange (new System.Windows.Forms.ColumnHeader [] {
            this.fileNameColumn,
            this.fileSizeColumn,
            this.fileDateColumn});
         this.listView.FullRowSelect = true;
         this.listView.GridLines = true;
         this.listView.Location = new System.Drawing.Point (3, 3);
         this.listView.MultiSelect = false;
         this.listView.Name = "listView";
         this.listView.Size = new System.Drawing.Size (225, 416);
         this.listView.TabIndex = 0;
         this.listView.UseCompatibleStateImageBehavior = false;
         this.listView.View = System.Windows.Forms.View.Details;
         this.listView.ItemActivate += new System.EventHandler (this.listView_ItemActivate);
         this.listView.DoubleClick += new System.EventHandler (this.listView_DoubleClick);
         this.listView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler (this.listView_ColumnClick);
         // 
         // fileNameColumn
         // 
         this.fileNameColumn.Text = "Name";
         this.fileNameColumn.Width = 80;
         // 
         // fileSizeColumn
         // 
         this.fileSizeColumn.Text = "Size";
         this.fileSizeColumn.Width = 80;
         // 
         // fileDateColumn
         // 
         this.fileDateColumn.Text = "Date";
         this.fileDateColumn.Width = 80;
         // 
         // tabControl
         // 
         this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.tabControl.Controls.Add (this.textTab);
         this.tabControl.Controls.Add (this.pictureTab);
         this.tabControl.Controls.Add (this.htmlTab);
         this.tabControl.Controls.Add (this.xmlTab);
         this.tabControl.Controls.Add (this.dataTab);
         this.tabControl.Location = new System.Drawing.Point (3, 3);
         this.tabControl.Name = "tabControl";
         this.tabControl.SelectedIndex = 0;
         this.tabControl.Size = new System.Drawing.Size (454, 416);
         this.tabControl.TabIndex = 0;
         // 
         // textTab
         // 
         this.textTab.Controls.Add (this.richTextBox);
         this.textTab.Location = new System.Drawing.Point (4, 22);
         this.textTab.Name = "textTab";
         this.textTab.Padding = new System.Windows.Forms.Padding (3);
         this.textTab.Size = new System.Drawing.Size (446, 390);
         this.textTab.TabIndex = 0;
         this.textTab.Text = "Text";
         this.textTab.UseVisualStyleBackColor = true;
         // 
         // richTextBox
         // 
         this.richTextBox.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.richTextBox.Location = new System.Drawing.Point (3, 3);
         this.richTextBox.Name = "richTextBox";
         this.richTextBox.Size = new System.Drawing.Size (447, 391);
         this.richTextBox.TabIndex = 0;
         this.richTextBox.Text = "";
         // 
         // pictureTab
         // 
         this.pictureTab.Controls.Add (this.pictureBox);
         this.pictureTab.Location = new System.Drawing.Point (4, 22);
         this.pictureTab.Name = "pictureTab";
         this.pictureTab.Padding = new System.Windows.Forms.Padding (3);
         this.pictureTab.Size = new System.Drawing.Size (446, 390);
         this.pictureTab.TabIndex = 1;
         this.pictureTab.Text = "Picture";
         this.pictureTab.UseVisualStyleBackColor = true;
         // 
         // pictureBox
         // 
         this.pictureBox.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.pictureBox.Location = new System.Drawing.Point (0, 3);
         this.pictureBox.Name = "pictureBox";
         this.pictureBox.Size = new System.Drawing.Size (450, 391);
         this.pictureBox.TabIndex = 0;
         this.pictureBox.TabStop = false;
         // 
         // htmlTab
         // 
         this.htmlTab.Controls.Add (this.webBrowser);
         this.htmlTab.Location = new System.Drawing.Point (4, 22);
         this.htmlTab.Name = "htmlTab";
         this.htmlTab.Padding = new System.Windows.Forms.Padding (3);
         this.htmlTab.Size = new System.Drawing.Size (446, 390);
         this.htmlTab.TabIndex = 2;
         this.htmlTab.Text = "HTML";
         this.htmlTab.UseVisualStyleBackColor = true;
         // 
         // webBrowser
         // 
         this.webBrowser.Anchor = ((System.Windows.Forms.AnchorStyles) ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.webBrowser.Location = new System.Drawing.Point (4, 4);
         this.webBrowser.MinimumSize = new System.Drawing.Size (20, 20);
         this.webBrowser.Name = "webBrowser";
         this.webBrowser.Size = new System.Drawing.Size (445, 389);
         this.webBrowser.TabIndex = 0;
         // 
         // dataTab
         // 
         this.dataTab.Controls.Add (this.dataGridView);
         this.dataTab.Location = new System.Drawing.Point (4, 22);
         this.dataTab.Name = "dataTab";
         this.dataTab.Padding = new System.Windows.Forms.Padding (3);
         this.dataTab.Size = new System.Drawing.Size (446, 390);
         this.dataTab.TabIndex = 3;
         this.dataTab.Text = "Data";
         this.dataTab.UseVisualStyleBackColor = true;
         // 
         // dataGridView
         // 
         this.dataGridView.AllowUserToOrderColumns = true;
         this.dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles) (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
         this.dataGridView.Location = new System.Drawing.Point (-4, 3);
         this.dataGridView.Name = "dataGridView";
         this.dataGridView.Size = new System.Drawing.Size (450, 391);
         this.dataGridView.TabIndex = 0;
         // 
         // xmlTab
         // 
         this.xmlTab.Controls.Add (this.treeBox);
         this.xmlTab.Location = new System.Drawing.Point (4, 22);
         this.xmlTab.Name = "xmlTab";
         this.xmlTab.Padding = new System.Windows.Forms.Padding (3);
         this.xmlTab.Size = new System.Drawing.Size (446, 390);
         this.xmlTab.TabIndex = 4;
         this.xmlTab.Text = "XML";
         this.xmlTab.UseVisualStyleBackColor = true;
         // 
         // treeBox
         // 
         this.treeBox.Location = new System.Drawing.Point (3, 1);
         this.treeBox.Name = "treeBox";
         this.treeBox.Size = new System.Drawing.Size (447, 393);
         this.treeBox.TabIndex = 0;
         // 
         // TreeForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF (6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size (1047, 490);
         this.Controls.Add (this.firstSplitter);
         this.Controls.Add (this.statusBar);
         this.Controls.Add (this.toolBar);
         this.Controls.Add (this.menuBar);
         this.MainMenuStrip = this.menuBar;
         this.Name = "TreeForm";
         this.Text = "Form1";
         this.toolBar.ResumeLayout (false);
         this.toolBar.PerformLayout ();
         this.menuBar.ResumeLayout (false);
         this.menuBar.PerformLayout ();
         this.statusBar.ResumeLayout (false);
         this.statusBar.PerformLayout ();
         this.firstSplitter.Panel1.ResumeLayout (false);
         this.firstSplitter.Panel2.ResumeLayout (false);
         this.firstSplitter.ResumeLayout (false);
         this.secondSplitter.Panel1.ResumeLayout (false);
         this.secondSplitter.Panel2.ResumeLayout (false);
         this.secondSplitter.ResumeLayout (false);
         this.tabControl.ResumeLayout (false);
         this.textTab.ResumeLayout (false);
         this.pictureTab.ResumeLayout (false);
         ((System.ComponentModel.ISupportInitialize) (this.pictureBox)).EndInit ();
         this.htmlTab.ResumeLayout (false);
         this.dataTab.ResumeLayout (false);
         ((System.ComponentModel.ISupportInitialize) (this.dataGridView)).EndInit ();
         this.xmlTab.ResumeLayout (false);
         this.ResumeLayout (false);
         this.PerformLayout ();

      }

      #endregion

      private System.Windows.Forms.ToolStrip toolBar;
      private System.Windows.Forms.MenuStrip menuBar;
      private System.Windows.Forms.StatusStrip statusBar;
      private System.Windows.Forms.ToolStripStatusLabel statusField;
      private System.Windows.Forms.SplitContainer firstSplitter;
      private System.Windows.Forms.TreeView treeView;
      private System.Windows.Forms.SplitContainer secondSplitter;
      private System.Windows.Forms.ListView listView;
      private System.Windows.Forms.TabControl tabControl;
      private System.Windows.Forms.TabPage textTab;
      private System.Windows.Forms.TabPage pictureTab;
      private System.Windows.Forms.ToolStripMenuItem fileMenu;
      private System.Windows.Forms.ToolStripMenuItem runMenuItem;
      private System.Windows.Forms.ToolStripButton runButton;
      private System.Windows.Forms.ColumnHeader fileNameColumn;
      private System.Windows.Forms.ColumnHeader fileSizeColumn;
      private System.Windows.Forms.ColumnHeader fileDateColumn;
      private System.Windows.Forms.RichTextBox richTextBox;
      private System.Windows.Forms.PictureBox pictureBox;
      private System.Windows.Forms.TabPage htmlTab;
      private System.Windows.Forms.WebBrowser webBrowser;
      private System.Windows.Forms.TabPage dataTab;
      private System.Windows.Forms.DataGridView dataGridView;
      private System.Windows.Forms.TabPage xmlTab;
      private System.Windows.Forms.TreeView treeBox;
   }
}

