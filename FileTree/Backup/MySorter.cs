﻿using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FileTree
{
   class MySorter : System.Collections.IComparer // NOT System.Collections.Generic.IComparer <ListViewItem>
   {
         public int column_index = 0;

         public bool inv = false;

         public int Compare (object x, object y)
         {
            MyListNode a = x as MyListNode;
            MyListNode b = y as MyListNode;
            int result = 0;
            switch (column_index)
            {
               case 1:
                  if (a.FileSize < b.FileSize)
                     result = -1;
                  else if ( a.FileSize == b.FileSize )
                     result =  0;
                  else
                     result =  1;
                  break;

               case 2:
                  result = DateTime.Compare (a.FileTime, b.FileTime);
                  break;

               default:
                  result = String.Compare (a.FileName, b.FileName);
                  break;
            }

            if ( inv )
               result = -result;

            return result;
         }
   }

}
