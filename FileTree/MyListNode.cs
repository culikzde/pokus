﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FileTree
{
   class MyListNode : ListViewItem
   {
      public string FilePath;
      public string FileName;
      public long FileSize;
      public DateTime FileTime;
   }
}
