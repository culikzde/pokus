﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FileTree
{
   class MyTreeNode : TreeNode
   {
      private string path;

      public string Path
      {
         get { return path; }
         set { path = value; }
      }

      public bool ready;
   }
}
