﻿using System;
using System.ComponentModel;

namespace Properties
{
    class MyObject
    {
        private int x;

        // using System.ComponentModel;
        [ Category ("Souřadnice") ]
        [ Description ("X-ová souřadnice") ]
        // [ Browsable (false) ]
        public int X
        {
            get { return x; }
            set { x = value; }
        }

        private string text;

        [Category("Texty")]
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

    }
}
