﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Properties
{
    public partial class Form1 : Form
    {
        private MyObject obj;

        public Form1()
        {
            InitializeComponent();
            obj = new MyObject ();
            obj.X = 7;
            obj.Text = "Abc";
            propertyGrid1.SelectedObject = obj;

        }

        private void runToolStripMenuItem_Click(object sender, EventArgs e)
        {
            listView1.ShowItemToolTips = true;

            ListViewItem item = new ListViewItem();
            item.Text = "Nejaky text";
            item.ToolTipText = "Nejaka napoveda";
            item.ForeColor = Color.Blue;
            item.SubItems.Add("AAA");
            item.SubItems.Add("BBB");
            listView1.Items.Add (item);
        }

        private void showControls (Control c)
        {
            foreach (Control s in c.Controls)
            {
                ListViewItem item = new ListViewItem();
                item.Text = s.Name;
                item.SubItems.Add (s.GetType().Name);
                item.Tag = s;
                listView1.Items.Add(item);

                showControls(s);
            }
        }

        private void controlsToolStripMenuItem_Click(object sender, EventArgs e)
        {
           showControls (this); 
        }

        private void listView1_ItemActivate(object sender, EventArgs e)
        {
            /*
            propertyGrid1.SelectedObject = null;

            object[] pole = new object[listView1.SelectedItems.Count];
            int inx = 0;
            foreach (ListViewItem item in listView1.SelectedItems)
            {
                pole[inx] = item;
                inx++;
            }

            propertyGrid1.SelectedObjects = pole;
            */

            if (listView1.SelectedItems.Count == 1)
                propertyGrid1.SelectedObject = 
                          listView1.SelectedItems[0].Tag;
            else
                propertyGrid1.SelectedObject = null;
        }

        [ Category ("Moje") ]
        // nezapomenout public
        public void typeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Type t = this.GetType();
            System.Reflection.MethodInfo [] m = t.GetMethods();
            foreach (System.Reflection.MethodInfo n in m)
            {
                // if (n.Name.StartsWith("t")) // mensi pocet metod
                {
                    ListViewItem item = new ListViewItem();
                    item.Text = n.Name;
                    item.ToolTipText = n.ToString();
                    // item.SubItems.Add(n.ReturnType.Name);
                    foreach (object a in n.GetCustomAttributes(true))
                        item.SubItems.Add(a.ToString());
                    item.Tag = n;
                    listView1.Items.Add(item);
                }
            }

        }


    }
}
