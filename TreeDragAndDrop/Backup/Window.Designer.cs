﻿namespace TreeObjects
{
   partial class Window
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose (bool disposing)
      {
         if ( disposing && (components != null) )
         {
            components.Dispose ();
         }
         base.Dispose (disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent ()
      {
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager (typeof (Window));
         this.mainMenu = new System.Windows.Forms.MenuStrip ();
         this.fileMenu = new System.Windows.Forms.ToolStripMenuItem ();
         this.testMenu = new System.Windows.Forms.ToolStripMenuItem ();
         this.quitMenu = new System.Windows.Forms.ToolStripMenuItem ();
         this.editMenu = new System.Windows.Forms.ToolStripMenuItem ();
         this.toolBar = new System.Windows.Forms.ToolStrip ();
         this.testButton = new System.Windows.Forms.ToolStripButton ();
         this.newTabButton = new System.Windows.Forms.ToolStripButton ();
         this.newItemButton = new System.Windows.Forms.ToolStripButton ();
         this.statusBar = new System.Windows.Forms.StatusStrip ();
         this.splitContainer1 = new System.Windows.Forms.SplitContainer ();
         this.tree = new System.Windows.Forms.TreeView ();
         this.splitContainer2 = new System.Windows.Forms.SplitContainer ();
         this.tabControl = new System.Windows.Forms.TabControl ();
         this.grid = new System.Windows.Forms.PropertyGrid ();
         this.mainMenu.SuspendLayout ();
         this.toolBar.SuspendLayout ();
         this.splitContainer1.Panel1.SuspendLayout ();
         this.splitContainer1.Panel2.SuspendLayout ();
         this.splitContainer1.SuspendLayout ();
         this.splitContainer2.Panel1.SuspendLayout ();
         this.splitContainer2.Panel2.SuspendLayout ();
         this.splitContainer2.SuspendLayout ();
         this.SuspendLayout ();
         // 
         // mainMenu
         // 
         this.mainMenu.Items.AddRange (new System.Windows.Forms.ToolStripItem [] {
            this.fileMenu,
            this.editMenu});
         this.mainMenu.Location = new System.Drawing.Point (0, 0);
         this.mainMenu.Name = "mainMenu";
         this.mainMenu.Size = new System.Drawing.Size (895, 24);
         this.mainMenu.TabIndex = 0;
         this.mainMenu.Text = "menuStrip1";
         // 
         // fileMenu
         // 
         this.fileMenu.DropDownItems.AddRange (new System.Windows.Forms.ToolStripItem [] {
            this.testMenu,
            this.quitMenu});
         this.fileMenu.Name = "fileMenu";
         this.fileMenu.Size = new System.Drawing.Size (35, 20);
         this.fileMenu.Text = "File";
         // 
         // testMenu
         // 
         this.testMenu.Name = "testMenu";
         this.testMenu.ShortcutKeys = ((System.Windows.Forms.Keys) ((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
         this.testMenu.Size = new System.Drawing.Size (145, 22);
         this.testMenu.Text = "Test";
         // 
         // quitMenu
         // 
         this.quitMenu.Name = "quitMenu";
         this.quitMenu.ShortcutKeys = ((System.Windows.Forms.Keys) ((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
         this.quitMenu.Size = new System.Drawing.Size (145, 22);
         this.quitMenu.Text = "Quit";
         this.quitMenu.Click += new System.EventHandler (this.quitMenu_Click);
         // 
         // editMenu
         // 
         this.editMenu.Name = "editMenu";
         this.editMenu.Size = new System.Drawing.Size (37, 20);
         this.editMenu.Text = "Edit";
         // 
         // toolBar
         // 
         this.toolBar.Items.AddRange (new System.Windows.Forms.ToolStripItem [] {
            this.testButton,
            this.newTabButton,
            this.newItemButton});
         this.toolBar.Location = new System.Drawing.Point (0, 24);
         this.toolBar.Name = "toolBar";
         this.toolBar.Size = new System.Drawing.Size (895, 25);
         this.toolBar.TabIndex = 1;
         this.toolBar.Text = "toolStrip1";
         // 
         // testButton
         // 
         this.testButton.Image = ((System.Drawing.Image) (resources.GetObject ("testButton.Image")));
         this.testButton.ImageTransparentColor = System.Drawing.Color.Magenta;
         this.testButton.Name = "testButton";
         this.testButton.Size = new System.Drawing.Size (48, 22);
         this.testButton.Text = "Test";
         // 
         // newTabButton
         // 
         this.newTabButton.Image = ((System.Drawing.Image) (resources.GetObject ("newTabButton.Image")));
         this.newTabButton.ImageTransparentColor = System.Drawing.Color.Magenta;
         this.newTabButton.Name = "newTabButton";
         this.newTabButton.Size = new System.Drawing.Size (69, 22);
         this.newTabButton.Text = "New Tab";
         this.newTabButton.MouseDown += new System.Windows.Forms.MouseEventHandler (this.newTabButton_MouseDown);
         // 
         // newItemButton
         // 
         this.newItemButton.Image = ((System.Drawing.Image) (resources.GetObject ("newItemButton.Image")));
         this.newItemButton.ImageTransparentColor = System.Drawing.Color.Magenta;
         this.newItemButton.Name = "newItemButton";
         this.newItemButton.Size = new System.Drawing.Size (73, 22);
         this.newItemButton.Text = "New Item";
         this.newItemButton.MouseDown += new System.Windows.Forms.MouseEventHandler (this.newItemButton_MouseDown);
         // 
         // statusBar
         // 
         this.statusBar.Location = new System.Drawing.Point (0, 560);
         this.statusBar.Name = "statusBar";
         this.statusBar.Size = new System.Drawing.Size (895, 22);
         this.statusBar.TabIndex = 2;
         this.statusBar.Text = "statusStrip1";
         // 
         // splitContainer1
         // 
         this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
         this.splitContainer1.Location = new System.Drawing.Point (0, 49);
         this.splitContainer1.Name = "splitContainer1";
         // 
         // splitContainer1.Panel1
         // 
         this.splitContainer1.Panel1.Controls.Add (this.tree);
         // 
         // splitContainer1.Panel2
         // 
         this.splitContainer1.Panel2.Controls.Add (this.splitContainer2);
         this.splitContainer1.Size = new System.Drawing.Size (895, 511);
         this.splitContainer1.SplitterDistance = 298;
         this.splitContainer1.TabIndex = 3;
         // 
         // tree
         // 
         this.tree.AllowDrop = true;
         this.tree.Dock = System.Windows.Forms.DockStyle.Fill;
         this.tree.Location = new System.Drawing.Point (0, 0);
         this.tree.Name = "tree";
         this.tree.Size = new System.Drawing.Size (298, 511);
         this.tree.TabIndex = 0;
         this.tree.DragDrop += new System.Windows.Forms.DragEventHandler (this.tree_DragDrop);
         this.tree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler (this.tree_AfterSelect);
         this.tree.DragEnter += new System.Windows.Forms.DragEventHandler (this.tree_DragEnter);
         this.tree.ItemDrag += new System.Windows.Forms.ItemDragEventHandler (this.tree_ItemDrag);
         this.tree.DragOver += new System.Windows.Forms.DragEventHandler (this.tree_DragOver);
         // 
         // splitContainer2
         // 
         this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
         this.splitContainer2.Location = new System.Drawing.Point (0, 0);
         this.splitContainer2.Name = "splitContainer2";
         // 
         // splitContainer2.Panel1
         // 
         this.splitContainer2.Panel1.Controls.Add (this.tabControl);
         // 
         // splitContainer2.Panel2
         // 
         this.splitContainer2.Panel2.Controls.Add (this.grid);
         this.splitContainer2.Size = new System.Drawing.Size (593, 511);
         this.splitContainer2.SplitterDistance = 313;
         this.splitContainer2.TabIndex = 0;
         // 
         // tabControl
         // 
         this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
         this.tabControl.Location = new System.Drawing.Point (0, 0);
         this.tabControl.Name = "tabControl";
         this.tabControl.SelectedIndex = 0;
         this.tabControl.Size = new System.Drawing.Size (313, 511);
         this.tabControl.TabIndex = 0;
         // 
         // grid
         // 
         this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
         this.grid.Location = new System.Drawing.Point (0, 0);
         this.grid.Name = "grid";
         this.grid.Size = new System.Drawing.Size (276, 511);
         this.grid.TabIndex = 0;
         // 
         // Window
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF (6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size (895, 582);
         this.Controls.Add (this.splitContainer1);
         this.Controls.Add (this.statusBar);
         this.Controls.Add (this.toolBar);
         this.Controls.Add (this.mainMenu);
         this.MainMenuStrip = this.mainMenu;
         this.Name = "Window";
         this.Text = "Form1";
         this.mainMenu.ResumeLayout (false);
         this.mainMenu.PerformLayout ();
         this.toolBar.ResumeLayout (false);
         this.toolBar.PerformLayout ();
         this.splitContainer1.Panel1.ResumeLayout (false);
         this.splitContainer1.Panel2.ResumeLayout (false);
         this.splitContainer1.ResumeLayout (false);
         this.splitContainer2.Panel1.ResumeLayout (false);
         this.splitContainer2.Panel2.ResumeLayout (false);
         this.splitContainer2.ResumeLayout (false);
         this.ResumeLayout (false);
         this.PerformLayout ();

      }

      #endregion

      private System.Windows.Forms.MenuStrip mainMenu;
      private System.Windows.Forms.ToolStripMenuItem fileMenu;
      private System.Windows.Forms.ToolStripMenuItem testMenu;
      private System.Windows.Forms.ToolStripMenuItem quitMenu;
      private System.Windows.Forms.ToolStripMenuItem editMenu;
      private System.Windows.Forms.ToolStrip toolBar;
      private System.Windows.Forms.ToolStripButton testButton;
      private System.Windows.Forms.StatusStrip statusBar;
      private System.Windows.Forms.SplitContainer splitContainer1;
      private System.Windows.Forms.TreeView tree;
      private System.Windows.Forms.SplitContainer splitContainer2;
      private System.Windows.Forms.TabControl tabControl;
      private System.Windows.Forms.ToolStripButton newTabButton;
      private System.Windows.Forms.ToolStripButton newItemButton;
      private System.Windows.Forms.PropertyGrid grid;

   }
}

