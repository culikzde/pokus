﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TreeObjects
{
   public class MyTabPage : TabPage
   {
      public ListView list;

      public MyTabPage ()
      {
         list = new ListView ();
         list.Parent = this;
         list.Dock = DockStyle.Fill;
         list.View = View.List;

         // list.Items.Add ("some item");
      }

   }
}
