﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TreeObjects
{
   public partial class Window : Form
   {
      public Window ()
      {
         InitializeComponent ();
         grid.PropertyTabs.AddTabType (typeof (MyPropertyTab));
         grid.PropertyTabs.AddTabType (typeof (MyEventTab));

         // textBox1.DataBindings.Add ("Text", textBox2, "Text");
         // testButton.DataBindings.Add ("Text", textBox, "Text");
      }

      private bool new_tab;
      private MyTreeNode source_node;
      private int tab_cnt = 0;
      private int item_cnt = 0;

      private void newTabButton_MouseDown (object sender, MouseEventArgs e)
      {
         new_tab = true;
         source_node = null;
         DoDragDrop ("some data", DragDropEffects.Copy);

      }
      
      private void newItemButton_MouseDown (object sender, MouseEventArgs e)
      {
         new_tab = false;
         source_node = null;
         DoDragDrop ("some data", DragDropEffects.Copy);
      }

      private void tree_DragEnter (object sender, DragEventArgs e)
      {
         e.Effect = e.AllowedEffect;
      }

      private void tree_DragOver (object sender, DragEventArgs e)
      {
         Point p = tree.PointToClient (new Point (e.X, e.Y));
         TreeNode n = tree.GetNodeAt (p);
         tree.SelectedNode = n;
      }

      private void tree_DragDrop (object sender, DragEventArgs e)
      {
         if ( source_node != null )
         {
            Point p = tree.PointToClient (new Point (e.X, e.Y));
            TreeNode target_node = tree.GetNodeAt (p);
            if ( target_node != null && 
                 source_node != target_node &&
                 // source_node.Parent != target_node &&
                 target_node.Parent != source_node )
            {
               bool target_tab = (target_node.Parent == null);

               bool source_tab = (source_node.Parent == null);
               if ( source_tab )
                  tree.Nodes.Remove (source_node);
               else
                  source_node.Parent.Nodes.Remove (source_node);

               if ( source_node.page != null )
               {
                  tabControl.TabPages.Remove (source_node.page);
                  source_node.page = null;
               }

               if ( source_node.item != null )
               {
                  source_node.item.ListView.Items.Remove (source_node.item);
                  source_node.item = null;
               }

               if ( source_tab )
               {
                  if (target_tab)
                     tree.Nodes.Insert (target_node.Index+1, source_node);
                  else
                     tree.Nodes.Insert (target_node.Parent.Index+1, source_node);
                 
               }
               else
               {
                  if (target_tab)
                     target_node.Nodes.Insert (0, source_node);
                  else
                     target_node.Parent.Nodes.Insert (target_node.Index+1, source_node);
               }


            }
         }
         else if ( new_tab )
         {
            Point p = tree.PointToClient (new Point (e.X, e.Y));
            TreeNode target = tree.GetNodeAt (p);
            int position = (target != null) ? target.Index + 1 : 0;

            TreeNode t = new MyTreeNode ();
            t.Text = "New Tab " + ++ tab_cnt;
            tree.Nodes.Insert (position, t);
            tree.SelectedNode = t;
         }
         else
         {
            TreeNode n = new MyTreeNode ();
            n.Text = "New Item " + ++ item_cnt;

            Point p = tree.PointToClient (new Point (e.X, e.Y));
            TreeNode t = tree.GetNodeAt (p);
            if ( t == null )
            {
               t = new MyTreeNode ();
               t.Text = "New Tab " + ++tab_cnt;
               tree.Nodes.Add (t);
               t.Nodes.Add (n);
            }
            else if ( t.Parent == null )
            {
               // add new item below t
               t.Nodes.Insert (0, n);
               t.Expand ();
            }
            else 
            {
               // add new item after t
               t.Parent.Nodes.Insert (t.Index+1, n);
               t.Parent.Expand ();
            }

            tree.SelectedNode = n;
         }

         updateTabs ();
      }

      private void updateTabs ()
      {
         foreach (MyTreeNode t in tree.Nodes)
         {
            if (t.page == null)
            {
               t.page = new MyTabPage ();
               t.page.Text = t.Text;
               tabControl.TabPages.Insert (t.Index, t.page);

               // t.page.DataBindings.Add ("Text", t, "Text");
               // t.page.DataBindings.Add ("BackColor", t, "BackColor");
            }

            foreach ( MyTreeNode n in t.Nodes )
            {
               if ( n.item == null )
               {
                  n.item = new ListViewItem ();
                  n.item.Text = n.Text;
                  t.page.list.Items.Insert (n.Index, n.item);
               }
            }
         }
      }

      private void tree_AfterSelect (object sender, TreeViewEventArgs e)
      {
         MyTreeInfo info = null;
         MyTreeNode node = tree.SelectedNode as MyTreeNode;
         if (node != null)
            info = node.info;

         // if ( info != null )
         //    textBox.DataBindings.Add ("Text", info, "Name");

         grid.SelectedObject = info;  
      }

      private void quitMenu_Click (object sender, EventArgs e)
      {
         Close ();
      }

      private void tree_ItemDrag (object sender, ItemDragEventArgs e)
      {
         // source_node = tree.SelectedNode as MyTreeNode;
         source_node = e.Item as MyTreeNode;
         DoDragDrop ("data", DragDropEffects.Move);
      }

   }
}
