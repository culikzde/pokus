﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace TreeObjects
{
   public class MyTreeInfo
   {
      public MyTreeNode node;

      [Browsable (true)]
      [Category ("Identification")]
      [Description ("node indentifier")]
      public string Name
      {
         get { return node.Id; }
         set { node.Id = value; }
      }

      [Browsable (true)]
      [Category ("Colors")]
      public System.Drawing.Color Color
      {
         get { return node.BackColor; }
         set { node.BackColor = value; }
      }
   }
}
