﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;

namespace TreeObjects
{
   // This component adds a TypeEventsTab to the Properties Window.
   // [PropertyTabAttribute (typeof (MyEventTab), PropertyTabScope.Document)]
   // [PropertyTabAttribute (typeof (MyPropTab), PropertyTabScope.Document)]
   public class MyTreeNode : TreeNode
   {
      public MyTreeInfo info;

      public MyTabPage page;
      public ListViewItem item;

      public MyTreeNode ()
      {
         info = new MyTreeInfo ();
         info.node = this;
      }


      // [Browsable (true)]
      // [Category ("Extra")]
      // [Description ("node indentifier")]
      public string Id
      {
         get { return Text; }
         set 
         { 
            Text = value;
            if ( page != null )
               page.Text = value;
            if ( item != null )
               item.Text = value;
         }
      }

      // [Browsable (true)]
      // [Category ("Colors")]
      public System.Drawing.Color Color
      {
         get { return BackColor; }
         set 
         { 
             BackColor = value;
             if ( page != null )
                page.BackColor = value;
             if ( item != null )
                item.BackColor = value;
         }
      }

   }


}
