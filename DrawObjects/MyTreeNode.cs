﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DrawObjects
{
   public class MyTreeNode : TreeNode
   {
      private MyObject data;

      public MyObject Data
      {
         get { return data; }
         set { data = value; }
      }
   }
}
