﻿namespace DrawObjects
{
   partial class MainForm
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose (bool disposing)
      {
         if ( disposing && (components != null) )
         {
            components.Dispose ();
         }
         base.Dispose (disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent ()
      {
         this.components = new System.ComponentModel.Container ();
         System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem ("Abc");
         System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem ("Klm");
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager (typeof (MainForm));
         this.menu = new System.Windows.Forms.MenuStrip ();
         this.fileMenu = new System.Windows.Forms.ToolStripMenuItem ();
         this.openDirMenuItem = new System.Windows.Forms.ToolStripMenuItem ();
         this.openXmlMenuItem = new System.Windows.Forms.ToolStripMenuItem ();
         this.saveXmlMenuItem = new System.Windows.Forms.ToolStripMenuItem ();
         this.quitMenuItem = new System.Windows.Forms.ToolStripMenuItem ();
         this.editMenu = new System.Windows.Forms.ToolStripMenuItem ();
         this.newObjectMenuItem = new System.Windows.Forms.ToolStripMenuItem ();
         this.newLineMenuItem = new System.Windows.Forms.ToolStripMenuItem ();
         this.viewMenu = new System.Windows.Forms.ToolStripMenuItem ();
         this.status = new System.Windows.Forms.StatusStrip ();
         this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel ();
         this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel ();
         this.mainArea = new System.Windows.Forms.PictureBox ();
         this.firstSplitContainer = new System.Windows.Forms.SplitContainer ();
         this.treeView = new System.Windows.Forms.TreeView ();
         this.listView = new System.Windows.Forms.ListView ();
         this.secondSplitContainer = new System.Windows.Forms.SplitContainer ();
         this.EditB = new System.Windows.Forms.TextBox ();
         this.EditA = new System.Windows.Forms.TextBox ();
         this.propertyGrid = new System.Windows.Forms.PropertyGrid ();
         this.toolbar = new System.Windows.Forms.ToolStrip ();
         this.newLineButton = new System.Windows.Forms.ToolStripButton ();
         this.newObjectButton = new System.Windows.Forms.ToolStripButton ();
         this.centralPanel = new System.Windows.Forms.Panel ();
         this.mainSplitContainer = new System.Windows.Forms.SplitContainer ();
         this.openFileDialog = new System.Windows.Forms.OpenFileDialog ();
         this.saveFileDialog = new System.Windows.Forms.SaveFileDialog ();
         this.contextMenu = new System.Windows.Forms.ContextMenuStrip (this.components);
         this.menu.SuspendLayout ();
         this.status.SuspendLayout ();
         ((System.ComponentModel.ISupportInitialize) (this.mainArea)).BeginInit ();
         this.firstSplitContainer.Panel1.SuspendLayout ();
         this.firstSplitContainer.Panel2.SuspendLayout ();
         this.firstSplitContainer.SuspendLayout ();
         this.secondSplitContainer.Panel1.SuspendLayout ();
         this.secondSplitContainer.Panel2.SuspendLayout ();
         this.secondSplitContainer.SuspendLayout ();
         this.toolbar.SuspendLayout ();
         this.centralPanel.SuspendLayout ();
         this.mainSplitContainer.Panel1.SuspendLayout ();
         this.mainSplitContainer.Panel2.SuspendLayout ();
         this.mainSplitContainer.SuspendLayout ();
         this.SuspendLayout ();
         // 
         // menu
         // 
         this.menu.Items.AddRange (new System.Windows.Forms.ToolStripItem [] {
            this.fileMenu,
            this.editMenu,
            this.viewMenu});
         this.menu.Location = new System.Drawing.Point (0, 0);
         this.menu.Name = "menu";
         this.menu.Size = new System.Drawing.Size (680, 24);
         this.menu.TabIndex = 0;
         this.menu.Text = "menuStrip1";
         // 
         // fileMenu
         // 
         this.fileMenu.DropDownItems.AddRange (new System.Windows.Forms.ToolStripItem [] {
            this.openDirMenuItem,
            this.openXmlMenuItem,
            this.saveXmlMenuItem,
            this.quitMenuItem});
         this.fileMenu.Name = "fileMenu";
         this.fileMenu.ShortcutKeyDisplayString = "";
         this.fileMenu.Size = new System.Drawing.Size (35, 20);
         this.fileMenu.Text = "&File";
         // 
         // openDirMenuItem
         // 
         this.openDirMenuItem.Name = "openDirMenuItem";
         this.openDirMenuItem.Size = new System.Drawing.Size (158, 22);
         this.openDirMenuItem.Text = "Open Directory";
         this.openDirMenuItem.Click += new System.EventHandler (this.openDirMenuItem_Click);
         // 
         // openXmlMenuItem
         // 
         this.openXmlMenuItem.Name = "openXmlMenuItem";
         this.openXmlMenuItem.Size = new System.Drawing.Size (158, 22);
         this.openXmlMenuItem.Text = "Open XML";
         this.openXmlMenuItem.Click += new System.EventHandler (this.openXmlMenuItem_Click);
         // 
         // saveXmlMenuItem
         // 
         this.saveXmlMenuItem.Name = "saveXmlMenuItem";
         this.saveXmlMenuItem.Size = new System.Drawing.Size (158, 22);
         this.saveXmlMenuItem.Text = "Save XML";
         this.saveXmlMenuItem.Click += new System.EventHandler (this.saveXmlMenuItem_Click);
         // 
         // quitMenuItem
         // 
         this.quitMenuItem.Name = "quitMenuItem";
         this.quitMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys) ((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
         this.quitMenuItem.Size = new System.Drawing.Size (158, 22);
         this.quitMenuItem.Text = "&Quit";
         this.quitMenuItem.Click += new System.EventHandler (this.quitMenuItem_Click);
         // 
         // editMenu
         // 
         this.editMenu.DropDownItems.AddRange (new System.Windows.Forms.ToolStripItem [] {
            this.newObjectMenuItem,
            this.newLineMenuItem});
         this.editMenu.Name = "editMenu";
         this.editMenu.Size = new System.Drawing.Size (37, 20);
         this.editMenu.Text = "&Edit";
         // 
         // newObjectMenuItem
         // 
         this.newObjectMenuItem.Name = "newObjectMenuItem";
         this.newObjectMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys) ((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
         this.newObjectMenuItem.Size = new System.Drawing.Size (181, 22);
         this.newObjectMenuItem.Text = "New Object";
         this.newObjectMenuItem.Click += new System.EventHandler (this.newObjectMenuItem_Click);
         // 
         // newLineMenuItem
         // 
         this.newLineMenuItem.Name = "newLineMenuItem";
         this.newLineMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys) ((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
         this.newLineMenuItem.Size = new System.Drawing.Size (181, 22);
         this.newLineMenuItem.Text = "New &Line";
         this.newLineMenuItem.Click += new System.EventHandler (this.newLineMenuItem_Click);
         // 
         // viewMenu
         // 
         this.viewMenu.Name = "viewMenu";
         this.viewMenu.Size = new System.Drawing.Size (41, 20);
         this.viewMenu.Text = "View";
         // 
         // status
         // 
         this.status.Items.AddRange (new System.Windows.Forms.ToolStripItem [] {
            this.statusLabel,
            this.toolStripStatusLabel1});
         this.status.Location = new System.Drawing.Point (0, 425);
         this.status.Name = "status";
         this.status.Size = new System.Drawing.Size (680, 22);
         this.status.TabIndex = 1;
         this.status.Text = "statusStrip1";
         // 
         // statusLabel
         // 
         this.statusLabel.BackColor = System.Drawing.Color.FromArgb (((int) (((byte) (128)))), ((int) (((byte) (128)))), ((int) (((byte) (255)))));
         this.statusLabel.Name = "statusLabel";
         this.statusLabel.Size = new System.Drawing.Size (0, 17);
         // 
         // toolStripStatusLabel1
         // 
         this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
         this.toolStripStatusLabel1.Size = new System.Drawing.Size (62, 17);
         this.toolStripStatusLabel1.Text = "statusLabel";
         // 
         // mainArea
         // 
         this.mainArea.BackColor = System.Drawing.Color.White;
         this.mainArea.Dock = System.Windows.Forms.DockStyle.Fill;
         this.mainArea.Location = new System.Drawing.Point (0, 0);
         this.mainArea.Name = "mainArea";
         this.mainArea.Size = new System.Drawing.Size (183, 376);
         this.mainArea.TabIndex = 2;
         this.mainArea.TabStop = false;
         // 
         // firstSplitContainer
         // 
         this.firstSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
         this.firstSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.firstSplitContainer.Location = new System.Drawing.Point (0, 0);
         this.firstSplitContainer.Name = "firstSplitContainer";
         // 
         // firstSplitContainer.Panel1
         // 
         this.firstSplitContainer.Panel1.Controls.Add (this.treeView);
         // 
         // firstSplitContainer.Panel2
         // 
         this.firstSplitContainer.Panel2.Controls.Add (this.listView);
         this.firstSplitContainer.Size = new System.Drawing.Size (327, 376);
         this.firstSplitContainer.SplitterDistance = 161;
         this.firstSplitContainer.TabIndex = 3;
         // 
         // treeView
         // 
         this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
         this.treeView.Location = new System.Drawing.Point (0, 0);
         this.treeView.Name = "treeView";
         this.treeView.Size = new System.Drawing.Size (161, 376);
         this.treeView.TabIndex = 0;
         this.treeView.MouseClick += new System.Windows.Forms.MouseEventHandler (this.treeView_MouseClick);
         this.treeView.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler (this.treeView_BeforeExpand);
         this.treeView.DoubleClick += new System.EventHandler (this.treeView_DoubleClick);
         this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler (this.treeView_AfterSelect);
         // 
         // listView
         // 
         this.listView.Dock = System.Windows.Forms.DockStyle.Fill;
         this.listView.GridLines = true;
         this.listView.Items.AddRange (new System.Windows.Forms.ListViewItem [] {
            listViewItem1,
            listViewItem2});
         this.listView.Location = new System.Drawing.Point (0, 0);
         this.listView.Name = "listView";
         this.listView.Size = new System.Drawing.Size (162, 376);
         this.listView.TabIndex = 0;
         this.listView.UseCompatibleStateImageBehavior = false;
         this.listView.View = System.Windows.Forms.View.List;
         this.listView.MouseClick += new System.Windows.Forms.MouseEventHandler (this.listView_MouseClick);
         this.listView.DoubleClick += new System.EventHandler (this.listView_DoubleClick);
         this.listView.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler (this.listView_ItemSelectionChanged);
         // 
         // secondSplitContainer
         // 
         this.secondSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
         this.secondSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
         this.secondSplitContainer.Location = new System.Drawing.Point (0, 0);
         this.secondSplitContainer.Name = "secondSplitContainer";
         // 
         // secondSplitContainer.Panel1
         // 
         this.secondSplitContainer.Panel1.Controls.Add (this.EditB);
         this.secondSplitContainer.Panel1.Controls.Add (this.EditA);
         this.secondSplitContainer.Panel1.Controls.Add (this.mainArea);
         // 
         // secondSplitContainer.Panel2
         // 
         this.secondSplitContainer.Panel2.Controls.Add (this.propertyGrid);
         this.secondSplitContainer.Size = new System.Drawing.Size (349, 376);
         this.secondSplitContainer.SplitterDistance = 183;
         this.secondSplitContainer.TabIndex = 0;
         // 
         // EditB
         // 
         this.EditB.Location = new System.Drawing.Point (71, 289);
         this.EditB.Name = "EditB";
         this.EditB.Size = new System.Drawing.Size (100, 20);
         this.EditB.TabIndex = 4;
         // 
         // EditA
         // 
         this.EditA.Location = new System.Drawing.Point (42, 238);
         this.EditA.Name = "EditA";
         this.EditA.Size = new System.Drawing.Size (100, 20);
         this.EditA.TabIndex = 3;
         // 
         // propertyGrid
         // 
         this.propertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
         this.propertyGrid.Location = new System.Drawing.Point (0, 0);
         this.propertyGrid.Name = "propertyGrid";
         this.propertyGrid.Size = new System.Drawing.Size (162, 376);
         this.propertyGrid.TabIndex = 0;
         // 
         // toolbar
         // 
         this.toolbar.Items.AddRange (new System.Windows.Forms.ToolStripItem [] {
            this.newLineButton,
            this.newObjectButton});
         this.toolbar.Location = new System.Drawing.Point (0, 24);
         this.toolbar.Name = "toolbar";
         this.toolbar.Size = new System.Drawing.Size (680, 25);
         this.toolbar.TabIndex = 4;
         this.toolbar.Text = "toolStrip1";
         // 
         // newLineButton
         // 
         this.newLineButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
         this.newLineButton.Name = "newLineButton";
         this.newLineButton.Size = new System.Drawing.Size (54, 22);
         this.newLineButton.Text = "New Line";
         this.newLineButton.Click += new System.EventHandler (this.newLineMenuItem_Click);
         // 
         // newObjectButton
         // 
         this.newObjectButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
         this.newObjectButton.Image = ((System.Drawing.Image) (resources.GetObject ("newObjectButton.Image")));
         this.newObjectButton.ImageTransparentColor = System.Drawing.Color.Magenta;
         this.newObjectButton.Name = "newObjectButton";
         this.newObjectButton.Size = new System.Drawing.Size (67, 22);
         this.newObjectButton.Text = "New Object";
         this.newObjectButton.Click += new System.EventHandler (this.newObjectMenuItem_Click);
         // 
         // centralPanel
         // 
         this.centralPanel.Controls.Add (this.mainSplitContainer);
         this.centralPanel.Dock = System.Windows.Forms.DockStyle.Fill;
         this.centralPanel.Location = new System.Drawing.Point (0, 49);
         this.centralPanel.Name = "centralPanel";
         this.centralPanel.Size = new System.Drawing.Size (680, 376);
         this.centralPanel.TabIndex = 5;
         // 
         // mainSplitContainer
         // 
         this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
         this.mainSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
         this.mainSplitContainer.Location = new System.Drawing.Point (0, 0);
         this.mainSplitContainer.Name = "mainSplitContainer";
         // 
         // mainSplitContainer.Panel1
         // 
         this.mainSplitContainer.Panel1.Controls.Add (this.firstSplitContainer);
         // 
         // mainSplitContainer.Panel2
         // 
         this.mainSplitContainer.Panel2.Controls.Add (this.secondSplitContainer);
         this.mainSplitContainer.Size = new System.Drawing.Size (680, 376);
         this.mainSplitContainer.SplitterDistance = 327;
         this.mainSplitContainer.TabIndex = 6;
         // 
         // openFileDialog
         // 
         this.openFileDialog.FileName = "openFileDialog1";
         // 
         // contextMenu
         // 
         this.contextMenu.Name = "contextMenu";
         this.contextMenu.Size = new System.Drawing.Size (153, 26);
         // 
         // MainForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF (6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size (680, 447);
         this.Controls.Add (this.centralPanel);
         this.Controls.Add (this.toolbar);
         this.Controls.Add (this.menu);
         this.Controls.Add (this.status);
         this.MainMenuStrip = this.menu;
         this.Name = "MainForm";
         this.Text = "Form1";
         this.menu.ResumeLayout (false);
         this.menu.PerformLayout ();
         this.status.ResumeLayout (false);
         this.status.PerformLayout ();
         ((System.ComponentModel.ISupportInitialize) (this.mainArea)).EndInit ();
         this.firstSplitContainer.Panel1.ResumeLayout (false);
         this.firstSplitContainer.Panel2.ResumeLayout (false);
         this.firstSplitContainer.ResumeLayout (false);
         this.secondSplitContainer.Panel1.ResumeLayout (false);
         this.secondSplitContainer.Panel1.PerformLayout ();
         this.secondSplitContainer.Panel2.ResumeLayout (false);
         this.secondSplitContainer.ResumeLayout (false);
         this.toolbar.ResumeLayout (false);
         this.toolbar.PerformLayout ();
         this.centralPanel.ResumeLayout (false);
         this.mainSplitContainer.Panel1.ResumeLayout (false);
         this.mainSplitContainer.Panel2.ResumeLayout (false);
         this.mainSplitContainer.ResumeLayout (false);
         this.ResumeLayout (false);
         this.PerformLayout ();

      }

      #endregion

      private System.Windows.Forms.MenuStrip menu;
      private System.Windows.Forms.StatusStrip status;
      private System.Windows.Forms.ToolStripMenuItem fileMenu;
      private System.Windows.Forms.ToolStripMenuItem quitMenuItem;
      private System.Windows.Forms.ToolStripMenuItem editMenu;
      private System.Windows.Forms.ToolStripMenuItem newLineMenuItem;
      private System.Windows.Forms.ToolStripStatusLabel statusLabel;
      private System.Windows.Forms.ToolStripMenuItem viewMenu;
      private System.Windows.Forms.SplitContainer firstSplitContainer;
      private System.Windows.Forms.SplitContainer secondSplitContainer;
      private System.Windows.Forms.TreeView treeView;
      private System.Windows.Forms.PropertyGrid propertyGrid;
      private System.Windows.Forms.ToolStrip toolbar;
      private System.Windows.Forms.ToolStripButton newLineButton;
      private System.Windows.Forms.ToolStripButton newObjectButton;
      private System.Windows.Forms.ToolStripMenuItem openDirMenuItem;
      private System.Windows.Forms.ToolStripMenuItem openXmlMenuItem;
      private System.Windows.Forms.Panel centralPanel;
      private System.Windows.Forms.SplitContainer mainSplitContainer;
      private System.Windows.Forms.ListView listView;
      private System.Windows.Forms.OpenFileDialog openFileDialog;
      private System.Windows.Forms.TextBox EditB;
      private System.Windows.Forms.TextBox EditA;
      private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
      private System.Windows.Forms.ToolStripMenuItem saveXmlMenuItem;
      private System.Windows.Forms.SaveFileDialog saveFileDialog;
      private System.Windows.Forms.ToolStripMenuItem newObjectMenuItem;
      public System.Windows.Forms.PictureBox mainArea;
      private System.Windows.Forms.ContextMenuStrip contextMenu;
   }
}

