﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;

namespace DrawObjects
{
   public class MyObject
   {
      private MyTreeNode treeNode;
      private MyListItem listItem;

      private List<MyObject> subitems = new List<MyObject> ();

      private Control view;
      private object parameters;

      private MainForm form;

      [Browsable (false)]
      public MyTreeNode TreeNode
      {
         get { return treeNode; }
         private set { treeNode = value; }
      }

      [Browsable (false)]
      public MyListItem ListItem
      {
         get { return listItem; }
         private set { listItem = value; }
      }

      [Browsable (false)]
      public List<MyObject> Subitems
      {
         get { return subitems; }
         set { subitems = value; }
      }

      [Browsable (false)]
      public Control View
      {
         get { return view; }
         protected set { view = value; }
      }

      [Browsable (false)]
      public object Parameters
      {
         get { return parameters; }
         set { parameters = value; }
      }

      [Browsable (false)]
      public MainForm Form
      {
         get { return form; }
         // set { form = value; }
      }

      private List<MyAction> actions = new List<MyAction> ();

      public List<MyAction> Actions
      {
         get { return actions; }
         set { actions = value; }
      }

      public void AddAction (string p_name, EventHandler p_proc)
      {
         MyAction a = new MyAction ();
         a.Name = p_name;
         a.Proc = p_proc;
         Actions.Add (a);
      }




      public MyObject (MainForm p_form)
      {
         form = p_form;
         // parameters = this;
      }

      virtual public string TreeNodeTitle ()
      {
         return "";
      }

      virtual public string ListItemTitle ()
      {
         return "";
      }

      public MyTreeNode CreateTreeNode ()
      {
         if ( TreeNode == null )
         {
            TreeNode = new MyTreeNode ();
            TreeNode.Data = this;
            TreeNode.Text = TreeNodeTitle ();
         }
         return TreeNode;
      }

      public MyListItem CreateListItem ()
      {
         if ( ListItem == null )
         {
            ListItem = new MyListItem ();
            ListItem.Data = this;
            ListItem.Text = ListItemTitle ();
         }
         return ListItem;
      }

      virtual public void DoubleClick ()
      {
         /* nothing */
      }

      virtual public void Selecting ()
      {
         /* nothing */
      }

      virtual public void Expanding ()
      {
         /* nothing */
      }
   }
}
