﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace DrawObjects
{
   public class Lines
   {
      private int cnt;
      private Control target;
      private Element [] elements;

      public delegate void redrawProcType ();
      public redrawProcType redrawProc;

      public int StartX;
      public int StartY;
      public int IncX = 8;
      public int IncY = 8;

      public ToolStripStatusLabel statusLabel;

      public int Cnt
      {
         get { return cnt; }
         set { cnt = value; }
      }

      public Control Target
      {
         get { return target; }
         set { target = value; }
      }


      public void Init ()
      {
         elements = new Element [cnt];
         for ( int i = 0; i < cnt; i++ )
         {
            Element e = new Element ();
            e.Location = new Point (StartX + IncX * i, StartY + IncY * i);
            e.Parent = target;
            e.controller = this; // for updates
            elements [i] = e;
         }
         Redraw ();
      }

      public void DrawLines (Graphics g)
      {
         if ( cnt >= 2 )
         {
            Point a = elements [0].Location;
            a.Offset (elements [0].Width / 2, elements [0].Height / 2);

            for ( int i = 0; i < cnt; i++ )
            {
               Point b = elements [i].Location;
               b.Offset (elements [i].Width / 2, elements [i].Height / 2);
               g.DrawLine (Pens.Blue, a, b);
               a = b;
            }
         }
         if ( statusLabel != null )
            statusLabel.Text = "DrawLines";
      }

      public void Redraw ()
      {
         if ( redrawProc != null )
            redrawProc ();
      }

   }
}
