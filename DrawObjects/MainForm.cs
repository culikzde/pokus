﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace DrawObjects
{
   public partial class MainForm : Form
   {
      private List<Lines> items;

      private static MainForm instance = null;

      public static MainForm Instance
      {
         get { return Instance; }
      }

      public MainForm ()
      {
         instance = this;

         InitializeComponent ();

         items = new List<Lines> ();
         mainArea.BackgroundImage = new Bitmap (mainArea.Width, mainArea.Height);

         EditA.DataBindings.Add ("Text", EditB, "Text", false, DataSourceUpdateMode.Never);


         OutputObject obj = new OutputObject (this);
         treeView.Nodes.Add (obj.CreateTreeNode ());
      }

      public void AddItem (Lines item)
      {
         items.Add (item);
      }

      public void DrawArea ()
      {
         Graphics g = Graphics.FromImage (mainArea.BackgroundImage);
         g.Clear (mainArea.BackColor);

         foreach ( Lines p in items )
            p.DrawLines (g);

         g.Dispose ();
         mainArea.Invalidate ();
      }

      private void quitMenuItem_Click (object sender, EventArgs e)
      {
         Close ();
      }

      private int lineCnt;
      
      private void newLineMenuItem_Click (object sender, EventArgs e)
      {
         Lines p = new Lines ();
         p.Cnt = 8; // number of Elemnets
         p.Target = mainArea; // container for Elements
         p.redrawProc = DrawArea;

         lineCnt++;
         p.StartX = 16;
         p.StartY = 16 * lineCnt;
         p.IncX = 16;
         p.IncY = 16;

         AddItem (p); // add to list of lines -- before Init ()
         p.Init (); // draw Elements
         p.statusLabel = statusLabel; // information output

         statusLabel.Text = "new line";
      }

      private void drawLinesMenuItem_Click (object sender, EventArgs e)
      {
         DrawArea ();
      }

      private void newObjectMenuItem_Click (object sender, EventArgs e)
      {
         ExampleObject obj = new ExampleObject (this);
         obj.Name = "New Object";
         treeView.Nodes.Add (obj.CreateTreeNode ());

      }

      /* Files and Directories */

      /*
      private void AddSubdir (TreeNode top, int level)
      {
         DirectoryInfo info = new DirectoryInfo (top.Text);

         DirectoryInfo [] dirs = info.GetDirectories ();
         foreach ( DirectoryInfo dir in dirs )
         {
            TreeNode node = new TreeNode ();
            node.Text = dir.Name;
            top.Nodes.Add (node);

            if ( level > 1 )
               AddSubdir (node, level-1); 
         }

         FileInfo [] files = info.GetFiles ();
         foreach ( FileInfo file in files )
         {
            TreeNode node = new TreeNode ();
            node.Text = file.Name;
            top.Nodes.Add (node);
         }
      }
      */

      private void openDirMenuItem_Click (object sender, EventArgs e)
      {
         /*
         TreeNode top = new TreeNode ();
         top.Text = "Drives";
         treeView.Nodes.Add (top);

         DriveInfo [] drives = DriveInfo.GetDrives ();
         foreach ( DriveInfo drive in drives )
         {
            TreeNode node = new TreeNode ();
            node.Text = drive.Name;
            top.Nodes.Add (node);
            if ( drive.Name == "C:\\" )
               AddSubdir (node, 1);
         }
         */

         TreeNode top = new TreeNode ();
         top.Text = "Disks";
         treeView.Nodes.Add (top);

         DriveInfo [] drives = DriveInfo.GetDrives ();
         foreach ( DriveInfo drive in drives )
         {
            DirectoryObject obj = new DirectoryObject (this);
            obj.Name = drive.Name;
            obj.Path = drive.Name;

            TreeNode node = obj.CreateTreeNode ();
            top.Nodes.Add (node);
         }

         top.Expand ();
      }

      /* XML */

      private void AddXmlNode (TreeNode top, XmlNode node)
      {
         TreeNode item = new TreeNode ();
         item.Tag = node;

         if (node is XmlElement)
            item.Text = node.Name;
         else
            item.Text = node.Value;

         if ( top == null )
            treeView.Nodes.Add (item);
         else
            top.Nodes.Add (item);

         foreach ( XmlNode n in node.ChildNodes )
            AddXmlNode (item, n);
      }

      private XmlDocument doc;

      private void openXmlMenuItem_Click (object sender, EventArgs e)
      {
         if ( openFileDialog.ShowDialog () == DialogResult.OK )
         {
            string fileName = openFileDialog.FileName;

            doc = new XmlDocument ();
            doc.Load (fileName);
            XmlElement elem = doc.DocumentElement;

            AddXmlNode (null, elem);
         }
      }

      private void saveXmlMenuItem_Click (object sender, EventArgs e)
      {
         if ( doc != null && saveFileDialog.ShowDialog () == DialogResult.OK )
         {
            string fileName = saveFileDialog.FileName;
            doc.Save (fileName);
         }

      }

      /* Form */

      private void SelectObject (MyObject data, bool also_list_view)
      {
         if ( data != null )
            data.Selecting ();
         
         /* list view */
         if ( also_list_view )
         {
            listView.Clear ();
            if ( data != null )
            {
               foreach ( MyObject p in data.Subitems )
               {
                  if ( p.ListItem == null )
                     p.CreateListItem ();
                  if ( p.ListItem != null )
                     listView.Items.Add (p.ListItem);
               }
            }
         }

         /* view panel */
         if (data != null)
         {
            Control p = data.View;
            if ( p != null )
               p.BringToFront ();
         }

         /* property grid */
         propertyGrid.SelectedObject = (data == null) ? null : data.Parameters;
      }

      private void DoubleClickObject (MyObject data)
      {
         if ( data != null )
            data.DoubleClick ();
      }

      private void ShowContextMenu (MyObject data)
      {
         if ( data != null && data.Actions.Count != 0)
         {
            contextMenu.Items.Clear ();
            Image image = null;
            foreach ( MyAction a in data.Actions )
               contextMenu.Items.Add (a.Name, image, a.Proc);
            contextMenu.Show (treeView, new Point (0, 0)); // !?

         }
      }

      public void ShowStatus (string s)
      {
         statusLabel.Text = s;
      }

      /* Tree */

      private MyObject CurrentTreeObject ()
      {
         MyObject d = null;
         MyTreeNode t = treeView.SelectedNode as MyTreeNode;
         if ( t != null )
            d = t.Data;
         return d;
      }

      private void treeView_AfterSelect (object sender, TreeViewEventArgs e)
      {
         SelectObject (CurrentTreeObject (), true);

         // for simple items
         TreeNode t = treeView.SelectedNode;
         if ( t != null && t.Tag != null )
            propertyGrid.SelectedObject = t.Tag;
      }

      private void treeView_MouseClick (object sender, MouseEventArgs e)
      {
         if ( e.Button == MouseButtons.Right )
            ShowContextMenu (CurrentTreeObject ());
      }

      private void treeView_DoubleClick (object sender, EventArgs e)
      {
         DoubleClickObject (CurrentTreeObject ());
      }

      private void treeView_BeforeExpand (object sender, TreeViewCancelEventArgs e)
      {
         MyObject data = CurrentTreeObject ();
         if ( data != null )
            data.Expanding ();
      }

      /* List */

      private MyObject CurrentListObject ()
      {
         MyObject d = null;
         if ( listView.SelectedItems.Count != 0 )
         {
            MyListItem t = listView.SelectedItems [0] as MyListItem;
            if ( t != null )
               d = t.Data;
         }
         return d;
      }

      private void listView_ItemSelectionChanged (object sender, ListViewItemSelectionChangedEventArgs e)
      {
         SelectObject (CurrentListObject (), false);
      }

      private void listView_MouseClick (object sender, MouseEventArgs e)
      {
         if ( e.Button == MouseButtons.Right )
            ShowContextMenu (CurrentListObject ());
      }

      private void listView_DoubleClick (object sender, EventArgs e)
      {
         DoubleClickObject (CurrentListObject ());

      }



   }
}
