﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Xml.Serialization;
using System.IO;

namespace DrawObjects
{
   public class ExampleObject : MyObject
   {
      private Color color = Color.Blue;

      [Description ("The color associated with the control"), Category ("Panel")] 
      public Color Color
      {
         get { return color; }
         set { color = value; }
      }

      private string name;

      [Description ("Button title"), Category ("Button")]
      public string Name
      {
         get { return name; }
         set { name = value; }
      }

      public override string TreeNodeTitle ()
      {
         return Name;
      }

      private string value;

      [Description ("Text box value"), Category ("Edit")]
      public string Value
      {
         get { return this.value; }
         set { this.value = value; }
      }


      public ExampleObject (MainForm p_form) :
         base (p_form)
      {
         Parameters = this;

         MyComponent panel = new MyComponent ();
         
         // panel.BackColor = Color;
         panel.DataBindings.Add ("BackColor", this, "Color");

         panel.button.DataBindings.Add ("Text", this, "Name");
         
         /*
         Binding b = new Binding ("Text", this, "Value");
         b.ControlUpdateMode = ControlUpdateMode.OnPropertyChanged;
         // b.ControlUpdateMode = ControlUpdateMode.Never;
         b.DataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged;
         panel.edit.DataBindings.Add (b);
         */

         panel.edit.DataBindings.Add ("Text", this, "Name");
         // this.DataBindings.Add ("Name", panel.edit, "Text");

         View = panel;
         View.Parent = Form.mainArea;

         AddAction ("Hello", HelloProc);
      }

      public ExampleObject () : this (null)
      {
      }

      public void HelloProc (Object sender, EventArgs e)
      {
         Form.ShowStatus ("Hello");
      }
           
   }
}
