﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DrawObjects
{
    public partial class MyComponent : UserControl
    {
        public MyComponent()
        {           
            InitializeComponent();
        }


        protected override CreateParams CreateParams
        {
          get
          {
               /*
               const int WS_OVERLAPPED       =  0x00000000;

               // const int WS_POPUP            = 0x80000000;
               const int WS_CHILD            = 0x40000000;
               const int WS_MINIMIZE         = 0x20000000;
               const int WS_VISIBLE          = 0x10000000;
               const int WS_DISABLED         = 0x08000000;
               const int WS_CLIPSIBLINGS     = 0x04000000;
               const int WS_CLIPCHILDREN     = 0x02000000;
               const int WS_MAXIMIZE         = 0x01000000;
               const int WS_CAPTION          = 0x00C00000;
               const int WS_BORDER           = 0x00800000;
               const int WS_DLGFRAME         = 0x00400000;
               const int WS_VSCROLL          = 0x00200000;
               const int WS_HSCROLL          = 0x00100000;
               const int WS_SYSMENU          = 0x00080000;
               const int WS_THICKFRAME       = 0x00040000;
               const int WS_GROUP            = 0x00020000;
               const int WS_TABSTOP          = 0x00010000;

               const int WS_MINIMIZEBOX      = 0x00020000;
               const int WS_MAXIMIZEBOX      = 0x00010000;

               const int WS_TILED            = WS_OVERLAPPED;
               const int WS_ICONIC           = WS_MINIMIZE;
               const int WS_SIZEBOX          = WS_THICKFRAME;
               */

               const int WS_THICKFRAME       = 0x00040000;
             
               CreateParams cp = base.CreateParams;
               cp.Style |= WS_THICKFRAME; 
               return cp;
          }

        }

        private bool stisk = false;
        private int X0;
        private int Y0;

        private void MyComponent_MouseDown(object sender, MouseEventArgs e)
        {
            stisk = true;
            X0 = e.X;
            Y0 = e.Y;
        }

        private void MyComponent_MouseMove(object sender, MouseEventArgs e)
        {
            if (stisk)
            {
                if (e.Button == MouseButtons.Left)
                {
                    int X = Location.X + e.X - X0;
                    int Y = Location.Y + e.Y - Y0;
                    Location = new Point(X, Y);
                }
                else
                {
                    int X = Size.Width + e.X - X0;
                    int Y = Size.Height + e.Y - Y0;
                    Size = new Size(X, Y);
                    X0 = e.X;
                    Y0 = e.Y;
                }
            }

        }

        private void MyComponent_MouseUp(object sender, MouseEventArgs e)
        {
            stisk = false;

        }
    }
}
