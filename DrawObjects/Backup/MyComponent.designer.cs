﻿namespace DrawObjects
{
    partial class MyComponent
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        /// 
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
           this.components = new System.ComponentModel.Container ();
           this.button = new System.Windows.Forms.Button ();
           this.propertyGrid1 = new System.Windows.Forms.PropertyGrid ();
           this.colorDialog1 = new System.Windows.Forms.ColorDialog ();
           this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip (this.components);
           this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem ();
           this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem ();
           this.edit = new System.Windows.Forms.TextBox ();
           this.contextMenuStrip1.SuspendLayout ();
           this.SuspendLayout ();
           // 
           // button
           // 
           this.button.Location = new System.Drawing.Point (15, 13);
           this.button.Name = "button";
           this.button.Size = new System.Drawing.Size (75, 23);
           this.button.TabIndex = 1;
           this.button.Text = "button1";
           this.button.UseVisualStyleBackColor = true;
           // 
           // propertyGrid1
           // 
           this.propertyGrid1.Location = new System.Drawing.Point (0, 0);
           this.propertyGrid1.Name = "propertyGrid1";
           this.propertyGrid1.Size = new System.Drawing.Size (140, 400);
           this.propertyGrid1.TabIndex = 0;
           // 
           // contextMenuStrip1
           // 
           this.contextMenuStrip1.Items.AddRange (new System.Windows.Forms.ToolStripItem [] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2});
           this.contextMenuStrip1.Name = "contextMenuStrip1";
           this.contextMenuStrip1.Size = new System.Drawing.Size (180, 48);
           // 
           // toolStripMenuItem1
           // 
           this.toolStripMenuItem1.Name = "toolStripMenuItem1";
           this.toolStripMenuItem1.Size = new System.Drawing.Size (179, 22);
           this.toolStripMenuItem1.Text = "toolStripMenuItem1";
           // 
           // toolStripMenuItem2
           // 
           this.toolStripMenuItem2.Name = "toolStripMenuItem2";
           this.toolStripMenuItem2.Size = new System.Drawing.Size (179, 22);
           this.toolStripMenuItem2.Text = "toolStripMenuItem2";
           // 
           // edit
           // 
           this.edit.Location = new System.Drawing.Point (14, 64);
           this.edit.Name = "edit";
           this.edit.Size = new System.Drawing.Size (75, 20);
           this.edit.TabIndex = 2;
           // 
           // MyComponent
           // 
           this.BackColor = System.Drawing.Color.Lime;
           this.ContextMenuStrip = this.contextMenuStrip1;
           this.Controls.Add (this.edit);
           this.Controls.Add (this.button);
           this.Name = "MyComponent";
           this.Size = new System.Drawing.Size (156, 182);
           this.MouseMove += new System.Windows.Forms.MouseEventHandler (this.MyComponent_MouseMove);
           this.MouseDown += new System.Windows.Forms.MouseEventHandler (this.MyComponent_MouseDown);
           this.MouseUp += new System.Windows.Forms.MouseEventHandler (this.MyComponent_MouseUp);
           this.contextMenuStrip1.ResumeLayout (false);
           this.ResumeLayout (false);
           this.PerformLayout ();

        }

        #endregion

        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ColorDialog colorDialog1;
        public System.Windows.Forms.TextBox edit;
        public System.Windows.Forms.Button button;
    }
}
