﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Drawing;

namespace DrawObjects
{
   // http://www.codeproject.com
   // Using the XmlSerializer Attributes

   [XmlRoot ("OutData")]
   public class OutputData
   {
      private string name;

      [XmlElement ("Identifier")]
      public string Name
      {
         get { return name; }
         set { name = value; }
      }

      private int value = 7;

      // [XmlAttribute ("Number")]
      public int Value
      {
         get { return this.value; }
         set { this.value = value; }
      }

      /*
      private Color color = Color.Blue;

      public Color Color
      {
         get { return color; }
         set { color = value; }
      }
      */
   }
}
