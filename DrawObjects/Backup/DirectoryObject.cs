﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace DrawObjects
{
   public class DirectoryObject : MyObject
   {
      private string name;
      private string path;

      public string Name
      {
         get { return name; }
         set { name = value; }
      }

      public string Path
      {
         get { return path; }
         set { path = value; }
      }

      override public string TreeNodeTitle ()
      {
         return Name;
      }

      public DirectoryObject (MainForm p_form) :
         base (p_form)
      {
      }

      private bool scanned = false;
      private List<DirectoryObject> subdirectories = new List<DirectoryObject> ();

      private void Scan (int level)
      {
         if ( ! scanned )
         {
            scanned = true;
            try
            {
               DirectoryInfo info = new DirectoryInfo (Path);

               DirectoryInfo [] dirs = info.GetDirectories ();
               foreach ( DirectoryInfo dir in dirs )
               {
                  DirectoryObject obj = new DirectoryObject (Form);
                  obj.Name = dir.Name;
                  obj.Path = dir.FullName; // !?

                  MyTreeNode node = obj.CreateTreeNode ();
                  TreeNode.Nodes.Add (node);

                  subdirectories.Add (obj);
               }

               FileInfo [] files = info.GetFiles ();
               foreach ( FileInfo file in files )
               {
                  FileObject obj = new FileObject (Form);
                  obj.Name = file.Name;
                  obj.Path = file.FullName;

                  Subitems.Add (obj);
               }
            }
            catch ( Exception ex )
            {
               Form.ShowStatus (ex.ToString ());
            }
         }

         if ( level > 1 )
            foreach (DirectoryObject obj in subdirectories)
               obj.Scan (level-1);
      }

      override public void DoubleClick ()
      {
         Scan (2);
         TreeNode.Expand ();
      }

      override public void Selecting ()
      {
         Scan (2);
      }

      override public void Expanding ()
      {
         Scan (2);
      }
   }
}
