﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;

namespace DrawObjects
{
   class OutputObject : MyObject
   {
      private OutputData output_data = new OutputData ();

      public override string TreeNodeTitle ()
      {
         return output_data.Name;
      }

      public OutputObject (MainForm p_form) :
         base (p_form)
      {
         output_data.Name = "Output Object";

         Parameters = output_data;

         AddAction ("Load", LoadProc);
         AddAction ("Save", SaveProc);
      }

      public void LoadProc (Object sender, EventArgs e)
      {
         OpenFileDialog dlg = new OpenFileDialog ();
         if ( dlg.ShowDialog () == DialogResult.OK )
         {
            XmlSerializer reader = new XmlSerializer (output_data.GetType ());
            StreamReader stream = new StreamReader (dlg.FileName);
            output_data = (OutputData) reader.Deserialize (stream);
            stream.Close ();

            Parameters = output_data;
         }
      }

      public void SaveProc (Object sender, EventArgs e)
      {
         SaveFileDialog dlg = new SaveFileDialog ();
         if ( dlg.ShowDialog () == DialogResult.OK )
         {
            XmlSerializer writer = new XmlSerializer (output_data.GetType ());
            StreamWriter stream = new StreamWriter (dlg.FileName);
            writer.Serialize (stream, output_data);
            stream.Close ();
         }
      }


   }
}
