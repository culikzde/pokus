﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrawObjects
{
   public class MyAction
   {
      private string name;

      public string Name
      {
         get { return name; }
         set { name = value; }
      }

      // public delegate void MyProc (Object sender, EventArgs e);

      private EventHandler proc;

      internal EventHandler Proc
      {
         get { return proc; }
         set { proc = value; }
      }


   }
}
