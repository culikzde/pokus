﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrawObjects
{
   public class FileObject : MyObject
   {
      private string name;
      private string path;

      public string Name
      {
         get { return name; }
         set { name = value; }
      }

      public string Path
      {
         get { return path; }
         set { path = value; }
      }

      public FileObject (MainForm p_form) :
         base (p_form)
      {
      }

      public override string ListItemTitle ()
      {
         return Name;
      }

   }
}
