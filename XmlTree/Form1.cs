﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace XmlTree
{
    public partial class XmlForm : Form
    {
        public XmlForm()
        {
            InitializeComponent();
        }

        private void showInfo (string s)
        {
            statusLabel.Text = s;
            logBox.AppendText (s + "\r\n");
        }

        private void showObj (object p)
        {
            propertyGrid.SelectedObject = p;
        }

        TreeNode current = null;

        private void runButton_Click(object sender, EventArgs e)
        {
            // readSimpleXml("..\\..\\data.xml");
            readDom ("..\\..\\data.xml");
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
        }

        protected void showDom (XmlNode node, TreeNode top)
        {
            MyTreeNode n = new MyTreeNode ();
            n.Text = node.Name;
            n.obj = node;

            if (top == null)
                treeView.Nodes.Add (n);
            else
                top.Nodes.Add(n);

            foreach (XmlNode child in node.ChildNodes)
                showDom (child, n);
        }

        protected void readDom (string fileName)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load (fileName);
            showObj (doc.DocumentElement);
            showDom ( doc.DocumentElement, null);
        }

        // using System.Xml;
        XmlTextReader f;

        protected void readSimpleXml(string fileName)
        {
            f = new XmlTextReader("..\\..\\data.xml");
            while (f.Read())
            {
                showObj(f);
                if (f.NodeType == XmlNodeType.Element)
                {
                    TreeNode n = new TreeNode();
                    n.Text = "<" + f.Name + ">";
                    showInfo("Element: " + f.Name);

                    if (current == null)
                        treeView.Nodes.Add(n);
                    else
                        current.Nodes.Add(n);

                    current = n;

                    if (f.HasAttributes)
                    {
                        for (int i = 0; i < f.AttributeCount; i++)
                        {
                            f.MoveToAttribute(i);
                            TreeNode a = new TreeNode();
                            a.Text = f.Name + " = " + f.Value;
                            current.Nodes.Add(a);
                        }
                        f.MoveToElement();
                    }

                    if (f.IsEmptyElement)
                        current = current.Parent;
                }
                else if (f.NodeType == XmlNodeType.EndElement)
                {
                    current = current.Parent;
                }
                else if (f.NodeType == XmlNodeType.Text)
                {
                    TreeNode n = new TreeNode();
                    n.Text = f.Value;
                    current.Nodes.Add(n);
                }
            }
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode n = treeView.SelectedNode;
            MyTreeNode m = n as MyTreeNode;
            if (m != null)
                showObj (m.obj);
        }

    }
}
