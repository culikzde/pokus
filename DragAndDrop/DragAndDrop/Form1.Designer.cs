﻿namespace DragAndDrop
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sourcePanel = new System.Windows.Forms.Panel();
            this.targetPanel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // sourcePanel
            // 
            this.sourcePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.sourcePanel.Location = new System.Drawing.Point(144, 44);
            this.sourcePanel.Name = "sourcePanel";
            this.sourcePanel.Size = new System.Drawing.Size(200, 100);
            this.sourcePanel.TabIndex = 0;
            this.sourcePanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.sourcePanel_MouseDown);
            // 
            // targetPanel
            // 
            this.targetPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.targetPanel.Location = new System.Drawing.Point(211, 251);
            this.targetPanel.Name = "targetPanel";
            this.targetPanel.Size = new System.Drawing.Size(200, 100);
            this.targetPanel.TabIndex = 1;
            this.targetPanel.DragDrop += new System.Windows.Forms.DragEventHandler(this.targetPanel_DragDrop);
            this.targetPanel.DragEnter += new System.Windows.Forms.DragEventHandler(this.targetPanel_DragEnter);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.targetPanel);
            this.Controls.Add(this.sourcePanel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel sourcePanel;
        private System.Windows.Forms.Panel targetPanel;
    }
}

