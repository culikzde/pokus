﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DragAndDrop
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            targetPanel.AllowDrop = true;
        }

        private void sourcePanel_MouseDown(object sender, MouseEventArgs e)
        {
            // DoDragDrop("lime", DragDropEffects.All);
            DoDragDrop (sourcePanel, DragDropEffects.All);
        }

        private void targetPanel_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent (DataFormats.Text))
               e.Effect = DragDropEffects.Copy;
            else if (e.Data.GetDataPresent("System.Windows.Forms.Panel"))
                e.Effect = DragDropEffects.Move;
            else
                e.Effect = DragDropEffects.None;
        }

        private void targetPanel_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.Text))
            {
                string s = e.Data.GetData(DataFormats.Text).ToString();
                targetPanel.BackColor = Color.FromName(s);
            }
            else if (e.Data.GetDataPresent("System.Windows.Forms.Panel"))
            {
                Panel p = e.Data.GetData("System.Windows.Forms.Panel") as Panel;
                targetPanel.BackColor = p.BackColor;
            }

        }
    }
}
