﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace XmlApplication
{
    public partial class MyForm : Form
    {
        public MyForm()
        {
            InitializeComponent();
        }

        private void quitMenu_Click(object sender, EventArgs e)
        {
            Close();
        }

        private XmlDocument doc;

        private void AddNode(TreeNode top, XmlNode node)
        {
            TreeNode n = new TreeNode();
            n.Tag = node;

            if (node is XmlElement)
               n.Text = "<" + node.Name + ">";
            else
               n.Text = node.Value;

            if (top == null)
                treeView.Nodes.Add(n);
            else
                top.Nodes.Add(n);

            foreach (XmlNode sub_node in node.ChildNodes)
                AddNode(n, sub_node);
        }

        private void openMenu_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                doc = new XmlDocument();
                doc.Load(openFileDialog.FileName);
                XmlElement elem = doc.DocumentElement;
                AddNode (null, elem);
            }

        }

        private void saveMenu_Click(object sender, EventArgs e)
        {
            //
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode node = treeView.SelectedNode;
            if (node != null)
               propertyGrid.SelectedObject = node.Tag;
        }

    }
}
