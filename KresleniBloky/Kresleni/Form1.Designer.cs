﻿namespace Kresleni
{
    partial class Okno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolPanel = new System.Windows.Forms.Panel();
            this.numericField = new System.Windows.Forms.NumericUpDown();
            this.toolComboBox = new System.Windows.Forms.ComboBox();
            this.colorCheckBox = new System.Windows.Forms.CheckBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addColorPanelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addMyPanelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.objectPage = new System.Windows.Forms.TabPage();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.directoryPage = new System.Windows.Forms.TabPage();
            this.directoryTree = new System.Windows.Forms.TreeView();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.picturePage = new System.Windows.Forms.TabPage();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.filePage = new System.Windows.Forms.TabPage();
            this.fileList = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.propertyPage = new System.Windows.Forms.TabPage();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.functionPage = new System.Windows.Forms.TabPage();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.testDirectoriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericField)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.objectPage.SuspendLayout();
            this.directoryPage.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.picturePage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.filePage.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.propertyPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolPanel
            // 
            this.toolPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.toolPanel.Controls.Add(this.numericField);
            this.toolPanel.Controls.Add(this.toolComboBox);
            this.toolPanel.Controls.Add(this.colorCheckBox);
            this.toolPanel.Controls.Add(this.panel3);
            this.toolPanel.Controls.Add(this.panel2);
            this.toolPanel.Controls.Add(this.panel1);
            this.toolPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolPanel.Location = new System.Drawing.Point(0, 24);
            this.toolPanel.Name = "toolPanel";
            this.toolPanel.Size = new System.Drawing.Size(649, 32);
            this.toolPanel.TabIndex = 1;
            // 
            // numericField
            // 
            this.numericField.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.numericField.DecimalPlaces = 2;
            this.numericField.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericField.Location = new System.Drawing.Point(504, 3);
            this.numericField.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.numericField.Name = "numericField";
            this.numericField.Size = new System.Drawing.Size(60, 20);
            this.numericField.TabIndex = 4;
            this.numericField.Value = new decimal(new int[] {
            135,
            0,
            0,
            0});
            // 
            // toolComboBox
            // 
            this.toolComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.toolComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolComboBox.FormattingEnabled = true;
            this.toolComboBox.Items.AddRange(new object[] {
            "Rectangle",
            "Ellipse",
            "Pie"});
            this.toolComboBox.Location = new System.Drawing.Point(377, 3);
            this.toolComboBox.Name = "toolComboBox";
            this.toolComboBox.Size = new System.Drawing.Size(121, 21);
            this.toolComboBox.TabIndex = 3;
            // 
            // colorCheckBox
            // 
            this.colorCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.colorCheckBox.AutoSize = true;
            this.colorCheckBox.Checked = true;
            this.colorCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.colorCheckBox.Location = new System.Drawing.Point(570, 5);
            this.colorCheckBox.Name = "colorCheckBox";
            this.colorCheckBox.Size = new System.Drawing.Size(74, 17);
            this.colorCheckBox.TabIndex = 2;
            this.colorCheckBox.Text = "two colors";
            this.colorCheckBox.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.panel3.Location = new System.Drawing.Point(78, 5);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(31, 21);
            this.panel3.TabIndex = 1;
            this.panel3.DoubleClick += new System.EventHandler(this.panel1_DoubleClick);
            this.panel3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Blue;
            this.panel2.Location = new System.Drawing.Point(41, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(31, 21);
            this.panel2.TabIndex = 1;
            this.panel2.DoubleClick += new System.EventHandler(this.panel1_DoubleClick);
            this.panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Red;
            this.panel1.Location = new System.Drawing.Point(4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(31, 21);
            this.panel1.TabIndex = 0;
            this.panel1.DoubleClick += new System.EventHandler(this.panel1_DoubleClick);
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            // 
            // colorDialog
            // 
            this.colorDialog.Color = System.Drawing.Color.Blue;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(649, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.toolStripMenuItem1,
            this.quitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.openToolStripMenuItem.Text = "Open";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.saveToolStripMenuItem.Text = "Save";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(97, 6);
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.quitToolStripMenuItem.Text = "Quit";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addColorPanelToolStripMenuItem,
            this.addMyPanelToolStripMenuItem,
            this.testDirectoriesToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.M)));
            this.editToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // addColorPanelToolStripMenuItem
            // 
            this.addColorPanelToolStripMenuItem.Name = "addColorPanelToolStripMenuItem";
            this.addColorPanelToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.addColorPanelToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.addColorPanelToolStripMenuItem.Text = "Add Color Panel";
            this.addColorPanelToolStripMenuItem.Click += new System.EventHandler(this.addColorPanelToolStripMenuItem_Click);
            // 
            // addMyPanelToolStripMenuItem
            // 
            this.addMyPanelToolStripMenuItem.Name = "addMyPanelToolStripMenuItem";
            this.addMyPanelToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.M)));
            this.addMyPanelToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.addMyPanelToolStripMenuItem.Text = "Add My Panel";
            this.addMyPanelToolStripMenuItem.Click += new System.EventHandler(this.addMyPanelToolStripMenuItem_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 56);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel2.Controls.Add(this.tabControl3);
            this.splitContainer1.Size = new System.Drawing.Size(649, 333);
            this.splitContainer1.SplitterDistance = 405;
            this.splitContainer1.TabIndex = 7;
            // 
            // splitContainer2
            // 
            this.splitContainer2.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer2.Panel1.Controls.Add(this.tabControl1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.tabControl2);
            this.splitContainer2.Size = new System.Drawing.Size(405, 333);
            this.splitContainer2.SplitterDistance = 134;
            this.splitContainer2.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.objectPage);
            this.tabControl1.Controls.Add(this.directoryPage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(134, 333);
            this.tabControl1.TabIndex = 7;
            // 
            // objectPage
            // 
            this.objectPage.Controls.Add(this.treeView1);
            this.objectPage.Location = new System.Drawing.Point(4, 22);
            this.objectPage.Name = "objectPage";
            this.objectPage.Padding = new System.Windows.Forms.Padding(3);
            this.objectPage.Size = new System.Drawing.Size(126, 307);
            this.objectPage.TabIndex = 0;
            this.objectPage.Text = "Objects";
            this.objectPage.UseVisualStyleBackColor = true;
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.Location = new System.Drawing.Point(3, 3);
            this.treeView1.Name = "treeView1";
            this.treeView1.ShowNodeToolTips = true;
            this.treeView1.Size = new System.Drawing.Size(120, 301);
            this.treeView1.TabIndex = 6;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // directoryPage
            // 
            this.directoryPage.Controls.Add(this.directoryTree);
            this.directoryPage.Location = new System.Drawing.Point(4, 22);
            this.directoryPage.Name = "directoryPage";
            this.directoryPage.Padding = new System.Windows.Forms.Padding(3);
            this.directoryPage.Size = new System.Drawing.Size(126, 307);
            this.directoryPage.TabIndex = 1;
            this.directoryPage.Text = "Directories";
            this.directoryPage.UseVisualStyleBackColor = true;
            // 
            // directoryTree
            // 
            this.directoryTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.directoryTree.Location = new System.Drawing.Point(3, 3);
            this.directoryTree.Name = "directoryTree";
            this.directoryTree.ShowNodeToolTips = true;
            this.directoryTree.Size = new System.Drawing.Size(120, 301);
            this.directoryTree.TabIndex = 0;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.picturePage);
            this.tabControl2.Controls.Add(this.filePage);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(0, 0);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(267, 333);
            this.tabControl2.TabIndex = 2;
            // 
            // picturePage
            // 
            this.picturePage.Controls.Add(this.pictureBox);
            this.picturePage.Location = new System.Drawing.Point(4, 22);
            this.picturePage.Name = "picturePage";
            this.picturePage.Padding = new System.Windows.Forms.Padding(3);
            this.picturePage.Size = new System.Drawing.Size(259, 307);
            this.picturePage.TabIndex = 0;
            this.picturePage.Text = "Picture";
            this.picturePage.UseVisualStyleBackColor = true;
            // 
            // pictureBox
            // 
            this.pictureBox.BackColor = System.Drawing.Color.White;
            this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox.Location = new System.Drawing.Point(3, 3);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(253, 301);
            this.pictureBox.TabIndex = 1;
            this.pictureBox.TabStop = false;
            this.pictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseMove);
            this.pictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseDown);
            this.pictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseUp);
            // 
            // filePage
            // 
            this.filePage.Controls.Add(this.fileList);
            this.filePage.Location = new System.Drawing.Point(4, 22);
            this.filePage.Name = "filePage";
            this.filePage.Padding = new System.Windows.Forms.Padding(3);
            this.filePage.Size = new System.Drawing.Size(259, 307);
            this.filePage.TabIndex = 1;
            this.filePage.Text = "Files";
            this.filePage.UseVisualStyleBackColor = true;
            // 
            // fileList
            // 
            this.fileList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.fileList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fileList.Location = new System.Drawing.Point(3, 3);
            this.fileList.Name = "fileList";
            this.fileList.Size = new System.Drawing.Size(253, 301);
            this.fileList.TabIndex = 0;
            this.fileList.UseCompatibleStateImageBehavior = false;
            this.fileList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Width = 59;
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.propertyPage);
            this.tabControl3.Controls.Add(this.functionPage);
            this.tabControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl3.Location = new System.Drawing.Point(0, 0);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(240, 333);
            this.tabControl3.TabIndex = 8;
            // 
            // propertyPage
            // 
            this.propertyPage.Controls.Add(this.propertyGrid1);
            this.propertyPage.Location = new System.Drawing.Point(4, 22);
            this.propertyPage.Name = "propertyPage";
            this.propertyPage.Padding = new System.Windows.Forms.Padding(3);
            this.propertyPage.Size = new System.Drawing.Size(232, 307);
            this.propertyPage.TabIndex = 0;
            this.propertyPage.Text = "Properties";
            this.propertyPage.UseVisualStyleBackColor = true;
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid1.Location = new System.Drawing.Point(3, 3);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(226, 301);
            this.propertyGrid1.TabIndex = 7;
            // 
            // functionPage
            // 
            this.functionPage.Location = new System.Drawing.Point(4, 22);
            this.functionPage.Name = "functionPage";
            this.functionPage.Padding = new System.Windows.Forms.Padding(3);
            this.functionPage.Size = new System.Drawing.Size(232, 307);
            this.functionPage.TabIndex = 1;
            this.functionPage.Text = "Functions";
            this.functionPage.UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 389);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(649, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // testDirectoriesToolStripMenuItem
            // 
            this.testDirectoriesToolStripMenuItem.Name = "testDirectoriesToolStripMenuItem";
            this.testDirectoriesToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.testDirectoriesToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.testDirectoriesToolStripMenuItem.Text = "Test Directories";
            this.testDirectoriesToolStripMenuItem.Click += new System.EventHandler(this.testDirectoriesToolStripMenuItem_Click);
            // 
            // Okno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 411);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolPanel);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.statusStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Okno";
            this.Text = "Kresleni";
            this.toolPanel.ResumeLayout(false);
            this.toolPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericField)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.objectPage.ResumeLayout(false);
            this.directoryPage.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.picturePage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.filePage.ResumeLayout(false);
            this.tabControl3.ResumeLayout(false);
            this.propertyPage.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel toolPanel;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addColorPanelToolStripMenuItem;
        private System.Windows.Forms.CheckBox colorCheckBox;
        private System.Windows.Forms.ComboBox toolComboBox;
        private System.Windows.Forms.NumericUpDown numericField;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ToolStripMenuItem addMyPanelToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage objectPage;
        private System.Windows.Forms.TabPage directoryPage;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage picturePage;
        private System.Windows.Forms.TabPage filePage;
        private System.Windows.Forms.ListView fileList;
        private System.Windows.Forms.TreeView directoryTree;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage propertyPage;
        private System.Windows.Forms.TabPage functionPage;
        private System.Windows.Forms.StatusStrip statusStrip1;
        public System.Windows.Forms.TreeView treeView1;
        public System.Windows.Forms.PictureBox pictureBox;
        public System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.ToolStripMenuItem testDirectoriesToolStripMenuItem;
    }
}

