﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.IO;


namespace Kresleni
{
    public partial class Okno : Form
    {
        private Bitmap bitmap;

        private MyArea area;

        public Okno()
        {
            InitializeComponent();
            int w = pictureBox.Width;
            int h = pictureBox.Height;
            bitmap = new Bitmap(w, h);
            Graphics g = Graphics.FromImage(bitmap);
            g.FillRectangle (Brushes.White, 0, 0, w-1, h-1);
            pictureBox.Image = bitmap;
            // pictureBox.Invalidate();

            toolComboBox.SelectedIndex = 1;

            area = new MyArea(this);
        }


        private void addMyPanelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MyPanel p = new MyPanel(area);
            // pictureBox.Controls.Add(p);
            // p.myWindow = this;
            // my_panel_cnt ++;
            // registerItem(myPanelSubTree, p, "panel " + my_panel_cnt);
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode node = treeView1.SelectedNode;
            if (node != null)
                propertyGrid1.SelectedObject = node.Tag;
            else
                propertyGrid1.SelectedObject = null;
        }

        private void selector_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
            if (selector.SelectedIndex == 0)
                propertyGrid1.SelectedObject = myPanel1;
            else if (selector.SelectedIndex == 1)
                propertyGrid1.SelectedObject = myPanel1.button1;
             */
        }

        int X0, Y0;
        bool stisknuto = false;

        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            stisknuto = true;
            X0 = e.X;
            Y0 = e.Y;
        }

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (stisknuto)
            {
                Graphics g = Graphics.FromHwnd(pictureBox.Handle);

                int X1 = e.X;
                int Y1 = e.Y;

                int X = X0 < X1 ? X0 : X1;
                int Y = Math.Min(Y0, Y1);

                int W = Math.Abs(X0 - X1);
                int H = Math.Abs(Y0 - Y1);

                Pen pen = new Pen(penColor);

                g.DrawImage(pictureBox.Image, new Point(0, 0));
            }
        }

        private void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (stisknuto)
            {
                stisknuto = false;
                Graphics g = Graphics.FromImage(pictureBox.Image);
                int X1 = e.X;
                int Y1 = e.Y;

                int X = X0 < X1 ? X0 : X1;
                int Y = Math.Min (Y0, Y1); 

                int W = Math.Abs (X0 - X1);
                int H = Math.Abs (Y0 - Y1);

                Brush b = null;

                if (colorCheckBox.Checked)
                {
                    // using System.Drawing.Drawing2D;
                    b = new LinearGradientBrush
                        (new Point(X, Y), new Point(X + W - 1, Y + H - 1),
                         color, color2);
                }
                else
                {
                    b = new SolidBrush(color);
                }

                Pen pen = new Pen(penColor);
                pen.Width = 5;

                string s = toolComboBox.Text;
                int n = toolComboBox.SelectedIndex;

                if (n == 0)
                {
                     g.FillRectangle(b, X, Y, W, H);
                     g.DrawRectangle(pen, X, Y, W, H);
                }
                else if (n == 1)
                {
                    g.FillEllipse(b, X, Y, W, H);
                    g.DrawEllipse(pen, X, Y, W, H);
                }
                else if (n == 2)
                {
                    float start = 0;
                    float end = (float)numericField.Value;
                    g.FillPie (b, X, Y, W, H, start, end);
                    g.DrawPie (pen, X, Y, W, H, start, end);
                }

                pictureBox.Invalidate();
            }
        }

        private Color penColor = Color.Yellow;
        private Color color = Color.Blue;
        private Color color2 = Color.Red;

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            Control p = (Control) sender;
            if (e.Button == MouseButtons.Right)
                penColor = p.BackColor;
            else if (e.Button == MouseButtons.Middle)
                color2 = p.BackColor;
            else
                color = p.BackColor;
        }

        private void panel1_DoubleClick(object sender, EventArgs e)
        {
            Control p = (Control)sender;
            colorDialog.Color = p.BackColor;
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                p.BackColor = colorDialog.Color;
                color = p.BackColor;
            }
        }

        int panel_cnt = 0;

        private void addColorPanelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Panel p = new Panel ();
            
            // p.Parent = toolPanel;
            toolPanel.Controls.Add(p);

            p.BackColor = Color.CornflowerBlue;

            p.Width = panel3.Width;
            p.Height = panel3.Height;

            panel_cnt ++;
            p.Left = panel3.Left + panel_cnt * (panel3.Left - panel2.Left);
            p.Top = panel3.Top;

            p.MouseDown += panel1_MouseDown;
            p.DoubleClick += panel1_DoubleClick;

            // p.Show ();
        }

        private void pictureBox_SizeChanged(object sender, EventArgs e)
        {
            int w = Math.Max (pictureBox.Width, pictureBox.Image.Width);
            int h = Math.Max (pictureBox.Height, pictureBox.Image.Height);

            Bitmap b = new Bitmap(w, h);
            Graphics g = Graphics.FromImage(b);
            g.FillRectangle(Brushes.White, 0, 0, w-1, h-1);
            g.DrawImage(pictureBox.Image, new Point(0, 0));
            bitmap = b;
            pictureBox.Image = bitmap;
        }

        private void testDirectoriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // using System.IO;
            DirectoryInfo d = new DirectoryInfo ("c:\\");

            TreeNode node = new TreeNode();
            node.Text = d.Name;
            node.ToolTipText = d.FullName;
            directoryTree.Nodes.Add(node);

            DirectoryInfo [] subdirs = d.GetDirectories();
            foreach (DirectoryInfo s in subdirs)
            {
                TreeNode node2 = new TreeNode();
                node2.Text = s.Name;
                node2.ToolTipText = s.FullName;
                node.Nodes.Add(node2);
            }

            tabControl1.SelectedTab = directoryPage;
            directoryTree.ShowNodeToolTips = true;
        }

    }
}















