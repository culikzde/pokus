﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Kresleni
{
    public class MyArea
    {
        private Okno window;

        private int my_panel_cnt = 0;

        private TreeNode myPanelSubTree = null;

        public MyArea(Okno p_win)
        {
            window = p_win;

            myPanelSubTree = new TreeNode();
            myPanelSubTree.Text = "My Panels";
            myPanelSubTree.ToolTipText = "seznam vytvorenych panelu";
            myPanelSubTree.ForeColor = Color.CornflowerBlue;
            window.treeView1.Nodes.Add(myPanelSubTree);
        }

        public void selectItem(object obj)
        {
            window.propertyGrid1.SelectedObject = obj;
        }

        public TreeNode registerItem(TreeNode top, object obj, string name)
        {
            TreeNode node = new TreeNode();
            node.Text = name;
            node.ForeColor = Color.Orange;
            node.Tag = obj;
            if (top == null)
            {
                window.treeView1.Nodes.Add(node);
            }
            else
            {
                top.Nodes.Add(node);
                top.Expand();
            }

            selectItem (obj);
            return node;
        }


        public void addMyPanel(MyPanel p)
        {
            window.pictureBox.Controls.Add (p);

            my_panel_cnt ++;
            
            TreeNode top =
                registerItem (myPanelSubTree, 
                              p, 
                              "panel " + my_panel_cnt);

            for (int i = 0; i < 4; i++)
            {
                MyPoint t = p.points[i];
                window.pictureBox.Controls.Add (t);
                registerItem (top, t, "point " + i);
            }

        }
    }
}
