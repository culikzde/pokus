﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Kresleni
{
    public partial class MyPanel : UserControl
    {
        private MyArea area;
        public  MyPoint [] points;

        public MyPanel (MyArea p_area)
        {
            InitializeComponent();

            points = new MyPoint[4];
            for (int i = 0; i < 4; i++)
                points [i] = new MyPoint();

            area = p_area;
            area.addMyPanel(this);

            MyPanel_Move(null, null);
        }

        private void MyPanel_Move(object sender, EventArgs e)
        {
            MyPoint p = points[0];
            p.Left = Left - p.Width / 2;
            p.Top = Top - p.Height / 2;
            p.BringToFront();

            p = points[1];
            p.Left = Left + Width - p.Width / 2;
            p.Top = Top - p.Height / 2;
            p.BringToFront();

            p = points[2];
            p.Left = Left - p.Width / 2;
            p.Top = Top + Height - p.Height / 2;
            p.BringToFront();

            p = points[3];
            p.Left = Left + Width - p.Width / 2;
            p.Top = Top + Height - p.Height / 2;
            p.BringToFront();
        }

        private void MyPanel_Click(object sender, EventArgs e)
        {
            area.selectItem (this);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            area.selectItem (button1);

            // button1.BackColor = Color.Blue;
            // this.BackColor = Color.Red;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Bitmap b = new Bitmap(openFileDialog1.FileName);
                this.BackgroundImage = b;
            }
        }

        private int X0, Y0;
        private bool stisknuto = false;

        private void MyPanel_MouseDown(object sender, MouseEventArgs e)
        {
            stisknuto = true;
            X0 = e.X;
            Y0 = e.Y;
        }

        private void MyPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (stisknuto)
            {
                if (e.Button == MouseButtons.Left)
                {
                    int X = Location.X + e.X - X0;
                    int Y = Location.Y + e.Y - Y0;
                    Location = new Point(X, Y);
                }
                else if (e.Button == MouseButtons.Right)
                {
                    int W = Size.Width + e.X - X0;
                    int H = Size.Height + e.Y - Y0;
                    Size = new Size(W, H);
                    X0 = e.X;
                    Y0 = e.Y;
                }
            }
        }

        private void MyPanel_MouseUp(object sender, MouseEventArgs e)
        {
            stisknuto = false;
        }


    }
}
