﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using System.ComponentModel;

namespace Blocks
{
    public enum Size { small, middle, large };

    public class C
    {
        public C (Block b) { block = b; }

        private Block block;

        public Color Color { get => block.BackColor;
                             set => block.BackColor = value ; }

        [DisplayName("X-ova souradnice")]
        [Description("Neco ...")]
        public int X
        {
            get => block.Left;
            set => block.Left = value;
        }
         
        public int Y { 
            get => block.Top;
            set => block.Top = value;
        }

        public Point place
        {
            get => block.Location;
            set => block.Location = value;
        }

        public int Z { get; set; }

        
        public String  Text { get; set; }

        public Size Size { get; set; }
    }
}
