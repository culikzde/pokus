﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Blocks
{
    public partial class Block : UserControl
    {
        public Block()
        {
            InitializeComponent();
        }

        bool down = false;
        int X0, Y0;

        private void Block_MouseDown(object sender, MouseEventArgs e)
        {
            down = true;
            X0 = e.X;
            Y0 = e.Y;
            if (e.Button == MouseButtons.Middle)
            {
                colorDialog.Color = this.BackColor;
                if (colorDialog.ShowDialog() == DialogResult.OK)
                    this.BackColor = colorDialog.Color;
            }
        }
        private void Block_MouseMove(object sender, MouseEventArgs e)
        {
            if (down)
            {
                if (e.Button == MouseButtons.Left)
                {
                    this.Left += e.X - X0;
                    this.Top += e.Y - Y0;
                }
                else if (e.Button == MouseButtons.Right)
                {
                    Width += e.X - X0;
                    Height += e.Y - Y0;
                    X0 = e.X;
                    Y0 = e.Y;
                }
            }
        }

        private void Block_MouseUp(object sender, MouseEventArgs e)
        {
            down = false;
        }
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openDialog.ShowDialog () == DialogResult.OK)
            {
                string fileName = openDialog.FileName;
                this.BackgroundImage = new Bitmap (fileName);
                this.BackgroundImageLayout = ImageLayout.Stretch;
            }

        }


    }
}
