﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using System.ComponentModel;
using System.Xml.Serialization;

namespace Structure
{
    public enum Abc { a, bee, c };

    public class MyItem
    {
        [XmlAttribute ("N")]
        public String Name { get; set; }

        [XmlAttribute("X")]
        public int X { get; set; }

        [XmlAttribute("Y")]
        public int Y { get; set; }
    }
     
    [XmlRoot ("Something")]
    public class MyObject
    {
        private int number;
        [Category("Cisla")]
        [Description ("Nejake cislo")]
        [DisplayName ("Pocet prvku")]
        [XmlElement ("NumberOfItems")]
        public int Number
        {
            // int get () 
            // void set (int value);
            get { return number; }
            set { number = value; }
        }

        // [ Browsable (false) ]
        [XmlElement("ItemText")]
            public String Text { get; set; }

        public Color Barva { get; set; }
        // public Font TypPisma { get; set; }
        public Abc  abicko { get; set; }
        public Size velikost { get; set; }

        public List<MyItem> Seznam { get; set; }

        public MyObject()
        {
            Seznam = new List<MyItem> ();
            // Seznam.Add("first");
            // Seznam.Add("second");
        }
    }
}
