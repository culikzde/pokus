﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Structure
{
    public partial class Form1 : Form
    {
        private MyObject obj;

        public Form1()
        {
            InitializeComponent();
            treeView1.ShowNodeToolTips = true;
            display (treeView1.Nodes, this);

            obj = new MyObject ();
            propertyGrid1.SelectedObject = obj;

            showProperties (obj);

            test();
        }

        private void showProperties (object obj)
        {
           Type typ = obj.GetType();
           // using System.Reflection;
           PropertyInfo [] prop = typ.GetProperties();
           foreach (var p in prop)
           {
               String[] data = new String [3];
               data[0] = p.Name;
               data[1] = "" + p.GetValue (obj);
               String s = "";
               var attr = p.GetCustomAttributes();
               foreach (var a in attr)
               {
                   s = s + a + " ";
                   if (a is DisplayNameAttribute)
                   {
                       DisplayNameAttribute d = a as DisplayNameAttribute;
                       data [0] = d.DisplayName;
                   }
               }
               data[2] = s;
               dataGridView1.Rows.Add (data);
           }

        }

        private void display (TreeNodeCollection branch,
                              Control widget)
        {
            TreeNode node = new TreeNode ();
            node.Text = widget.Name + " : " +
                        widget.GetType().Name;
            node.ToolTipText = widget.ToString ();
            node.Tag = widget;
            branch.Add (node);

            foreach (Control c in widget.Controls)
                display (node.Nodes, c);
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close ();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            // propertyGrid1.SelectedObject = e.Node.Tag;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                // using System.IO;
                // using System.Xml.Serialization;
                StreamWriter stream = 
                    new StreamWriter (saveFileDialog1.FileName);
                XmlSerializer writer =
                    new XmlSerializer(typeof(MyObject));
                writer.Serialize(stream, obj);
                stream.Close();
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamReader stream =
                    new StreamReader (openFileDialog1.FileName);
                XmlSerializer reader =
                    new XmlSerializer(typeof(MyObject));
                obj = (MyObject) reader.Deserialize (stream);
                propertyGrid1.SelectedObject = obj;
                stream.Close();
            }

        }

        private void test()
        {
            var list = new List<MyObject>();

            var aa = new MyObject();
            aa.Text = "First";
            list.Add(aa);

            var b = new MyObject();
            b.Text = "Second";
            b.Number = 7;
            list.Add (b);

            Type typ = typeof (MyObject);
            PropertyInfo [] prop = typ.GetProperties();
            view.ColumnCount = prop.Length;
            int i = 0;
            foreach (var p in prop)
            {
                var c = view.Columns [i];
                c.Name = p.Name;
                var attr = p.GetCustomAttributes();
                foreach (var a in attr)
                    if (a is DisplayNameAttribute)
                    {
                        DisplayNameAttribute d = a as DisplayNameAttribute;
                        c.Name = d.DisplayName;
                    }
                i++;
            }
            


            foreach (MyObject item in list)
            {
               String[] data = new String [prop.Length];
               i = 0;
               foreach (var p in prop)
               {
                   data[i] = "" + p.GetValue (item);
                   i++;
               }
               view.Rows.Add(data);
            }
        }

    }
}
