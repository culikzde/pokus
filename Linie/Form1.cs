﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Linie
{
   public partial class Form1 : Form
   {
      private List<Lines> items;

      public Form1 ()
      {
         InitializeComponent ();

         items = new List<Lines> ();
         mainArea.BackgroundImage = new Bitmap (mainArea.Width, mainArea.Height);
      }

      public void AddItem (Lines item)
      {
         items.Add (item);
      }

      public void DrawArea ()
      {
         Graphics g = Graphics.FromImage (mainArea.BackgroundImage);
         g.Clear (mainArea.BackColor);

         foreach ( Lines p in items )
            p.DrawLines (g);

         g.Dispose ();
         mainArea.Invalidate ();
      }

      private void quitMenuItem_Click (object sender, EventArgs e)
      {
         Close ();
      }

      private void newLineMenuItem_Click (object sender, EventArgs e)
      {
         Lines p = new Lines ();
         p.Cnt = 8; // number of Elemnets
         p.Target = mainArea; // container for Elements
         p.redrawProc = DrawArea;
         AddItem (p); // add to list of lines -- before Init ()
         p.Init (); // draw Elements
         p.statusLabel = statusLabel; // information output

         statusLabel.Text = "new line";
      }

      private void drawLinesMenuItem_Click (object sender, EventArgs e)
      {
         DrawArea ();
      }
   }
}
