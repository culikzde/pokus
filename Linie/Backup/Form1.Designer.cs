﻿namespace Linie
{
   partial class Form1
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose (bool disposing)
      {
         if ( disposing && (components != null) )
         {
            components.Dispose ();
         }
         base.Dispose (disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent ()
      {
         this.menu = new System.Windows.Forms.MenuStrip ();
         this.fileMenu = new System.Windows.Forms.ToolStripMenuItem ();
         this.quitMenuItem = new System.Windows.Forms.ToolStripMenuItem ();
         this.editMenu = new System.Windows.Forms.ToolStripMenuItem ();
         this.newLineMenuItem = new System.Windows.Forms.ToolStripMenuItem ();
         this.viewMenu = new System.Windows.Forms.ToolStripMenuItem ();
         this.drawLinesMenuItem = new System.Windows.Forms.ToolStripMenuItem ();
         this.status = new System.Windows.Forms.StatusStrip ();
         this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel ();
         this.mainArea = new System.Windows.Forms.PictureBox ();
         this.menu.SuspendLayout ();
         this.status.SuspendLayout ();
         ((System.ComponentModel.ISupportInitialize) (this.mainArea)).BeginInit ();
         this.SuspendLayout ();
         // 
         // menu
         // 
         this.menu.Items.AddRange (new System.Windows.Forms.ToolStripItem [] {
            this.fileMenu,
            this.editMenu,
            this.viewMenu});
         this.menu.Location = new System.Drawing.Point (0, 0);
         this.menu.Name = "menu";
         this.menu.Size = new System.Drawing.Size (346, 24);
         this.menu.TabIndex = 0;
         this.menu.Text = "menuStrip1";
         // 
         // fileMenu
         // 
         this.fileMenu.DropDownItems.AddRange (new System.Windows.Forms.ToolStripItem [] {
            this.quitMenuItem});
         this.fileMenu.Name = "fileMenu";
         this.fileMenu.ShortcutKeyDisplayString = "";
         this.fileMenu.Size = new System.Drawing.Size (35, 20);
         this.fileMenu.Text = "&File";
         // 
         // quitMenuItem
         // 
         this.quitMenuItem.Name = "quitMenuItem";
         this.quitMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys) ((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
         this.quitMenuItem.Size = new System.Drawing.Size (145, 22);
         this.quitMenuItem.Text = "&Quit";
         this.quitMenuItem.Click += new System.EventHandler (this.quitMenuItem_Click);
         // 
         // editMenu
         // 
         this.editMenu.DropDownItems.AddRange (new System.Windows.Forms.ToolStripItem [] {
            this.newLineMenuItem});
         this.editMenu.Name = "editMenu";
         this.editMenu.Size = new System.Drawing.Size (37, 20);
         this.editMenu.Text = "&Edit";
         // 
         // newLineMenuItem
         // 
         this.newLineMenuItem.Name = "newLineMenuItem";
         this.newLineMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys) ((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
         this.newLineMenuItem.Size = new System.Drawing.Size (165, 22);
         this.newLineMenuItem.Text = "New &Line";
         this.newLineMenuItem.Click += new System.EventHandler (this.newLineMenuItem_Click);
         // 
         // viewMenu
         // 
         this.viewMenu.DropDownItems.AddRange (new System.Windows.Forms.ToolStripItem [] {
            this.drawLinesMenuItem});
         this.viewMenu.Name = "viewMenu";
         this.viewMenu.Size = new System.Drawing.Size (41, 20);
         this.viewMenu.Text = "View";
         // 
         // drawLinesMenuItem
         // 
         this.drawLinesMenuItem.Name = "drawLinesMenuItem";
         this.drawLinesMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys) ((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
         this.drawLinesMenuItem.Size = new System.Drawing.Size (176, 22);
         this.drawLinesMenuItem.Text = "Draw Lines";
         this.drawLinesMenuItem.Click += new System.EventHandler (this.drawLinesMenuItem_Click);
         // 
         // status
         // 
         this.status.Items.AddRange (new System.Windows.Forms.ToolStripItem [] {
            this.statusLabel});
         this.status.Location = new System.Drawing.Point (0, 281);
         this.status.Name = "status";
         this.status.Size = new System.Drawing.Size (346, 22);
         this.status.TabIndex = 1;
         this.status.Text = "statusStrip1";
         // 
         // statusLabel
         // 
         this.statusLabel.BackColor = System.Drawing.Color.FromArgb (((int) (((byte) (128)))), ((int) (((byte) (128)))), ((int) (((byte) (255)))));
         this.statusLabel.Name = "statusLabel";
         this.statusLabel.Size = new System.Drawing.Size (0, 17);
         // 
         // mainArea
         // 
         this.mainArea.BackColor = System.Drawing.Color.White;
         this.mainArea.Dock = System.Windows.Forms.DockStyle.Fill;
         this.mainArea.Location = new System.Drawing.Point (0, 24);
         this.mainArea.Name = "mainArea";
         this.mainArea.Size = new System.Drawing.Size (346, 257);
         this.mainArea.TabIndex = 2;
         this.mainArea.TabStop = false;
         // 
         // Form1
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF (6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size (346, 303);
         this.Controls.Add (this.mainArea);
         this.Controls.Add (this.status);
         this.Controls.Add (this.menu);
         this.MainMenuStrip = this.menu;
         this.Name = "Form1";
         this.Text = "Form1";
         this.menu.ResumeLayout (false);
         this.menu.PerformLayout ();
         this.status.ResumeLayout (false);
         this.status.PerformLayout ();
         ((System.ComponentModel.ISupportInitialize) (this.mainArea)).EndInit ();
         this.ResumeLayout (false);
         this.PerformLayout ();

      }

      #endregion

      private System.Windows.Forms.MenuStrip menu;
      private System.Windows.Forms.StatusStrip status;
      private System.Windows.Forms.ToolStripMenuItem fileMenu;
      private System.Windows.Forms.ToolStripMenuItem quitMenuItem;
      private System.Windows.Forms.ToolStripMenuItem editMenu;
      private System.Windows.Forms.ToolStripMenuItem newLineMenuItem;
      private System.Windows.Forms.ToolStripStatusLabel statusLabel;
      private System.Windows.Forms.ToolStripMenuItem viewMenu;
      private System.Windows.Forms.ToolStripMenuItem drawLinesMenuItem;
      private System.Windows.Forms.PictureBox mainArea;
   }
}

