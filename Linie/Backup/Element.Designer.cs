﻿namespace Linie
{
   partial class Element
   {
      /// <summary> 
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary> 
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose (bool disposing)
      {
         if ( disposing && (components != null) )
         {
            components.Dispose ();
         }
         base.Dispose (disposing);
      }

      #region Component Designer generated code

      /// <summary> 
      /// Required method for Designer support - do not modify 
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent ()
      {
         this.SuspendLayout ();
         // 
         // Element
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF (6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.BackColor = System.Drawing.Color.LemonChiffon;
         this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.Name = "Element";
         this.Size = new System.Drawing.Size (8, 8);
         this.MouseCaptureChanged += new System.EventHandler (this.Element_MouseCaptureChanged);
         this.MouseMove += new System.Windows.Forms.MouseEventHandler (this.Element_MouseMove);
         this.MouseDown += new System.Windows.Forms.MouseEventHandler (this.Element_MouseDown);
         this.MouseUp += new System.Windows.Forms.MouseEventHandler (this.Element_MouseUp);
         this.ResumeLayout (false);

      }

      #endregion
   }
}
