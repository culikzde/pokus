﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Linie
{
   /// <remarks></remarks>
   public partial class Element : UserControl
   {
      public Element ()
      {
         InitializeComponent ();
      }

      public Lines controller;

      private bool stisk = false;
      private int X0, Y0;
      private Color oldColor;

      private void Element_MouseDown (object sender, MouseEventArgs e)
      {
         stisk = true;
         X0 = e.X;
         Y0 = e.Y;
      
         oldColor = BackColor;
         BackColor = Color.Red;

         Capture = true;
         
      }

      private void Element_MouseMove (object sender, MouseEventArgs e)
      {
         if ( stisk )
         {
            int X = this.Location.X + e.X - X0;
            int Y = this.Location.Y + e.Y - Y0;
            this.Location = new Point (X, Y);

            if ( controller != null )
               controller.Redraw ();
         }
      }

      private void Element_MouseUp (object sender, MouseEventArgs e)
      {
         stisk = false;
         BackColor = oldColor;

         if ( controller != null )
            controller.Redraw ();
      }

      private void Element_MouseCaptureChanged (object sender, EventArgs e)
      {
      }
   }
}
